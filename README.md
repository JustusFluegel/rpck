# Minecraft RPCK

Minecraft RPCK is an ressource pack manager for minecraft, introducing simple formats for creating custom blocks, items and more, then compiling those source files into minecraft compatible ressource packs.

## Get started


To get started, simply install mc-rpck from npm: 

```bash
npm i -g mc-rpck
// OR
yarn global add mc-rpck
```

## Create Ressource pack

To create your first ressource pack simply enter 

```bash
rpck create project
//OR:
rpck c p
//OR:
rpck cp
```

In the folder you desire to create a project in.
Then the menu will take you trough further steps

## Create Items

With 
```bash
rpck create item
```
You can retexture already impemented minecraft items, or with 
```bash
rpck create custom-item
```
You can create own items.

## Further Steps

Until our documentation is complete, as long as you understand a little bit of programming, you should defenetly look into src/cli.js. There you can find any command that's currently (not, work in progress) implemented.