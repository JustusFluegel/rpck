{
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "project": "tsconfig.json",
        "ecmaVersion": 2020,
        "sourceType": "module"
    },
    "plugins": ["@typescript-eslint", "filenames", "fp", "import", "jsdoc"],
    "rules": {
        "newline-before-return": "error",
        "no-trailing-spaces": [
            "error",
            { "ignoreComments": true }
        ],
        "comma-dangle": ["error", {
            "arrays": "always-multiline",
            "objects": "always-multiline",
            "imports": "always-multiline",
            "exports": "always-multiline",
            "functions": "always-multiline"
        }],
        "semi": ["error", "always"],
        "require-await": "error",
        "curly": ["error", "multi-or-nest"],
        "arrow-parens": ["error", "as-needed"],
        "no-param-reassign": "error",
        "@typescript-eslint/no-non-null-assertion": "error",
        "no-magic-numbers": ["error",{"ignore": [1, 0, -1], "ignoreArrayIndexes": true, "enforceConst": true}],
        "@typescript-eslint/prefer-ts-expect-error": "error",
        "@typescript-eslint/consistent-type-assertions": [
            "error",
            {
                "assertionStyle": "as",
                "objectLiteralTypeAssertions": "never"
            }
        ],
        "@typescript-eslint/consistent-type-definitions": ["error", "interface"],
        "@typescript-eslint/explicit-member-accessibility": [
            "error",
            {
                "accessibility": "no-public",
                "overrides": {
                    "parameterProperties": "explicit"
                }
            }
        ],
        "@typescript-eslint/member-ordering": [
            "error",
            {
                "default": [
                    "static-field",
                    "field",
                    "public-static-method",
                    "constructor",
                    "method",
                    "protected-method",
                    "private-method"
                ]
            }
        ],
        "@typescript-eslint/no-empty-interface": "error",
        "@typescript-eslint/no-explicit-any": "error",
        "@typescript-eslint/no-extraneous-class": "error",
        "@typescript-eslint/no-for-in-array": "error",
        "@typescript-eslint/no-inferrable-types": [
            "error",
            {
                "ignoreProperties": true,
                "ignoreParameters": true
            }
        ],
        "@typescript-eslint/no-misused-new": "error",
        "@typescript-eslint/no-require-imports": "error",
        "@typescript-eslint/no-this-alias": [
            "error",
            {
                "allowDestructuring": true
            }
        ],
        "@typescript-eslint/no-unnecessary-qualifier": "error",
        "@typescript-eslint/no-useless-constructor": "error",
        "@typescript-eslint/prefer-function-type": "error",
        "@typescript-eslint/promise-function-async": [
            "error",
            {
                "allowAny": true
            }
        ],
        "@typescript-eslint/restrict-plus-operands": "error",
        "@typescript-eslint/triple-slash-reference": [
            "error",
            {
                "path": "never",
                "types": "never",
                "lib": "always"
            }
        ],
        // lots of false positives right now
        // '@typescript-eslint/unbound-method': 'error',
        "arrow-body-style": ["error", "as-needed"],
        "camelcase": "off",
        "@typescript-eslint/naming-convention": [
            "error",
            {
                "selector": "default",
                "format": ["camelCase"],
                "filter": {
                    "match":false,
                    "regex": "pack_format"
                }
            },

            {
                "selector": "variable",
                "format": ["camelCase", "PascalCase", "UPPER_CASE" ]
            },
            {
                "selector": "parameter",
                "format": ["camelCase"],
                "leadingUnderscore": "allow",
                "filter": {
                    "match":false,
                    "regex": "pack_format"
                }
            },
            {
                "selector": "memberLike",
                "format": ["camelCase", "UPPER_CASE"],
                "leadingUnderscore": "allow",
                "filter": {
                    "match":false,
                    "regex": "pack_format"
                }
                
            },
            {
                "selector": "memberLike",
                "modifiers": ["private"],
                "format": ["camelCase"],
                "leadingUnderscore": "require",
                "filter": {
                    "match":false,
                    "regex": "pack_format"
                }
            },
            {
                "selector": "enumMember",
                "format": ["camelCase", "PascalCase", "UPPER_CASE"],
                "filter": {
                    "match":false,
                    "regex": "pack_format"
                }
            },
            {
                "selector": "typeLike",
                "format": ["PascalCase"]
            },
            {
                "selector": "interface",
                "format": ["PascalCase"]
            }
        ],
        "consistent-return": "error",
        "default-case": "error",
        // not a thing right now
        // 'deprecation': 'error',
        "eqeqeq": [
            "error",
            "always",
            {
                "null": "ignore"
            }
        ],
        "eol-last": ["error", "always"],
        "filenames/match-exported": ["error",["camel"]],
        "fp/no-delete": "error",
        "guard-for-in": "error",
        "import/no-unassigned-import": "error",
        "jsdoc/check-alignment": "error",
        "jsdoc/check-indentation": "error",
        // errors with ts
        // 'jsdoc/check-param-names': 'error',
        // 'jsdoc/require-param': 'error',
        "jsdoc/check-tag-names": [
            "error",
            {
                "definedTags": ["inheritDoc", "expandParams", "hideProtected", "eventListener"]
            }
        ],
        "jsdoc/newline-after-description": ["error", "always"],
        "max-classes-per-file": ["error", 1],
        "no-bitwise": "error",
        "no-caller": "error",
        "no-cond-assign": "error",
        "no-console": ["error", {"allow": ["warn", "error", "trace"]}],
        "no-debugger": "error",
        "no-duplicate-case": "error",
        "no-duplicate-imports": "error",
        "no-empty": "error",
        "no-empty-character-class": "error",
        "no-eval": "error",
        "no-ex-assign": "error",
        "no-fallthrough": [
            "error",
            {
                "commentPattern": "fallthrough"
            }
        ],
        "no-irregular-whitespace": "error",
        "no-labels": "error",
        "no-negated-condition": "error",
        "no-new": "error",
        "no-new-wrappers": "error",
        "no-prototype-builtins": "error",
        "no-redeclare": "error",
        "no-restricted-syntax": [
            "error",
            {
                "selector":
                    "BinaryExpression[operator=/^[!=]==?$/][left.raw=/^(true|false)$/], BinaryExpression[operator=/^[!=]==?$/][right.raw=/^(true|false)$/]",
                "message": "Don't compare for equality against boolean literals"
            },
            {
                "selector": "SequenceExpression",
                "message": "The comma operator is confusing and a common mistake. Don’t use it!"
            }
        ],
        "no-return-await": "error",
        "no-shadow": "error",
        "no-sparse-arrays": "error",
        "no-throw-literal": "error",
        "no-unexpected-multiline": "error",
        "no-unneeded-ternary": "error",
        "no-unsafe-finally": "error",
        "no-use-before-define": "error",
        "no-var": "error",
        "prefer-arrow-callback": [
            "error",
            {
                "allowUnboundThis": false
            }
        ],
        "prefer-const": [
            "error",
            {
                "destructuring": "all",
                "ignoreReadBeforeAssign": true
            }
        ],
        "prefer-object-spread": "error",
        "prefer-rest-params": "error",
        "prefer-template": "error",
        "quotes": ["error", "double", { "avoidEscape": true }],
        "radix": ["error", "always"],
        "spaced-comment": [
            "error",
            "always",
            {
                "markers": ["*", "/"]
            }
        ],
        "use-isnan": "error",
        "yoda": ["error", "never"]
    }
}
