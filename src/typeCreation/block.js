const prompt = require("../prompt");
const fs = require("fs");
const uuidv3 = require("uuid/v3");
const crypto = require("crypto");

function checkDirExits(dir) {
  return new Promise(async (resolve, reject) => {
    try {
      await fs.promises.access(dir);
      resolve(true);
    } catch (err) {
      resolve(false);
    }
  });
}

const checkFileExists = checkDirExits;

exports.create = async (args, projectPath = "./") => {
  const config = await require("../getConfig")();

  let minecraftname = args[0];
  if (minecraftname == null) {
    console.log(
      "\x1b[32m%s\x1b[0m",
      "⌨ Please type the minecraft id (the part after minecraft:) of the block you want to texture:"
    );
    minecraftname = await prompt();

    if (minecraftname.trim() == "") {
      console.log(
        "\x1b[31m%s\x1b[0m",
        `❌ Error: The minecraft id sholdn't be empty.`
      );
      return;
    }
  }

  const blocks = require("../types/blocks");
  const blocknames = blocks.map(block => block.block);

  if (!blocknames.includes(minecraftname)) {
    console.log(
      "\x1b[1m\x1b[33m%s\x1b[0m",
      "⌨ The block isn't in our block list. Are you sure you want to proceed? (yes/no):"
    );
    const input = await prompt();
    if (input != "yes" && input != "y") {
      return;
    }
  }

  await require("../checkAndCreateStructureDirs")(config, projectPath);

  const dirExists = await checkDirExits(
    `${projectPath}${config.blocks}/${minecraftname}`
  );

  if (dirExists) {
    console.log(
      "\x1b[31m%s\x1b[0m",
      `❌ Error: The block is already re-textured. Please do just edit: \n - ${projectPath}${config.blocks}/${minecraftname}/${minecraftname}.png \n - ${projectPath}${config.blocks}/${minecraftname}/${minecraftname}.json`
    );
    return;
  }

  await fs.promises.mkdir(`${projectPath}${config.blocks}/${minecraftname}`);
  await fs.promises.writeFile(
    `${projectPath}${config.blocks}/${minecraftname}/${minecraftname}.json`,
    JSON.stringify(
      {
        item: minecraftname,
        texture: {
          type: "same",
          file: `./${minecraftname}.png`,
          "#Supported Types": ["same", "topBottom-seperated", "unique", "3D"]
        }
      },
      null,
      "\t"
    )
  );

  var img =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8\
    /9hAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUU\
    H4woEFQM0iaKKlwAAACRJREFUOMtjYGBg+P//////ZNMUaWZg+M8w6oJRF4y6YJC4A\
    AB8on2forAllwAAAABJRU5ErkJggg==";

  var data = img.replace(/^data:image\/\w+;base64,/, "");
  var buf = new Buffer.from(data, "base64");

  await fs.promises.writeFile(
    `${projectPath}${config.blocks}/${minecraftname}/${minecraftname}.png`,
    buf
  );
};
exports.bundle = async (dirs, projectPath = "./") => {
  const config = await require("../getConfig")();

  await require("./block/folder")(dirs, projectPath);

  const blockfiles = await fs.promises.readdir(
    `${projectPath}${config.blocks}/`
  );
  const blockfiles_stats = await Promise.all(
    blockfiles.map(block =>
      fs.promises.stat(`${projectPath}${config.blocks}/${block}`)
    )
  );
  const blockdirs_names = blockfiles.filter((_, i) =>
    blockfiles_stats[i].isDirectory()
  );
  const blockdirs = blockdirs_names.map(
    blockdir => `${projectPath}${config.blocks}/${blockdir}/`
  );

  const checkDirStructure = async (dir, dirname) => {
    const dirContents = await fs.promises.readdir(dir);
    const dirContents_stats = await Promise.all(
      dirContents.map(file => fs.promises.stat(dir + "/" + file))
    );

    if (!dirContents.includes(dirname + ".json")) return false;
    if (dirContents_stats[dirContents.indexOf(dirname + ".json")].isDirectory())
      return false;
    return true;
  };

  const checkBlockConfig = async (dir, dirname) => {
    const configContents_String = await fs.promises.readFile(
      dir + dirname + ".json"
    );
    try {
      const configContents = JSON.parse(configContents_String);
      if (configContents.texture == null) return false;
      if (configContents.texture.type == null) return false;
      if (
        configContents.texture.type != "3D" &&
        configContents.texture.type != "unique" &&
        configContents.texture.file == null
      ) {
        return false;
      }
      if (
        configContents.texture.type == "topBottom-seperated" &&
        (configContents.texture.topFile == null ||
          configContents.texture.bottomFile == null)
      )
        return false;
      if (
        configContents.texture.type == "unique" &&
        (configContents.texture.topFile == null ||
          configContents.texture.bottomFile == null ||
          configContents.texture.northFile == null ||
          configContents.texture.westFile == null ||
          configContents.texture.eastFile == null ||
          configContents.texture.southFile == null)
      )
        return false;

      if (
        configContents.texture.type == "3D" &&
        configContents.texture["3DData"] == null
      ) {
        return false;
      }

      return configContents;
    } catch (err) {
      return false;
    }
  };

  console.log("\x1b[32m%s\x1b[0m", `➡ Info: Bundling Blocks ...`);

  await Promise.all(
    blockdirs.map((dir, i) =>
      (async () => {
        const dirStructureOk = await checkDirStructure(dir, blockdirs_names[i]);
        if (!dirStructureOk) {
          console.log(
            "\x1b[33m%s\x1b[0m",
            `⚠ Warn: The block folder ${blockdirs_names[i]} hasn't his file ${blockdirs_names[i]}.json. Skipping...`
          );
          return;
        }

        const blockConfig = await checkBlockConfig(dir, blockdirs_names[i]);

        if (blockConfig === false) {
          console.log(
            "\x1b[33m%s\x1b[0m",
            `⚠ Warn: The block folder ${blockdirs_names[i]} has an error in his config "${blockdirs_names[i]}.json". Skipping...`
          );
          return;
        }

        const blockConfigTextures = blockConfig.texture;

        const {
          type,
          file,
          topFile,
          bottomFile,
          northFile,
          westFile,
          southFile,
          eastFile
        } = blockConfigTextures;
        const threeDData = blockConfigTextures["3DData"];

        const checkFileArrayExists = async fileArray =>
          Promise.all(
            Object.keys(fileArray).map(key =>
              (async () => {
                const fileExists = await checkFileExists(
                  `${projectPath}${config.blocks}/${
                    blockdirs_names[i]
                  }/${fileArray[key].split("./").slice(-1)}`
                );
                if (!fileExists) {
                  console.log(
                    "\x1b[33m%s\x1b[0m",
                    `\t⚠ \x1b[35m[${
                      blockdirs_names[i]
                    }]\x1b[32m  Warn: The block folder ${
                      blockdirs_names[i]
                    } has an error in his config "${
                      blockdirs_names[i]
                    }.json". The file ${fileArray[key]
                      .split("./")
                      .slice(-1)} doesn't exist. Skipping...`
                  );
                  return false;
                }
                return true;
              })()
            )
          );

        const copyFiles = async fileArray => {
          const dirExists = await checkDirExits(
            `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/block/${blockdirs_names[i]}/`
          );
          if (!dirExists) {
            await fs.promises.mkdir(
              `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/block/${blockdirs_names[i]}/`
            );
            console.log(
              "\x1b[32m%s\x1b[0m",
              `\t➡ \x1b[35m[${blockdirs_names[i]}]\x1b[32m Info: /assets/minecraft/textures/block/${blockdirs_names[i]} folder does not exist. Creating ...`
            );
          } else {
            console.log(
              "\x1b[32m%s\x1b[0m",
              `\t➡ \x1b[35m[${blockdirs_names[i]}]\x1b[32m Info: /assets/minecraft/textures/block/${blockdirs_names[i]} folder does exist. Skipping ...`
            );
          }
          const result = await Promise.all(
            Object.keys(fileArray).map(key =>
              (async () => {
                const fileContents = await fs.promises.readFile(
                  `${projectPath}${config.blocks}/${
                    blockdirs_names[i]
                  }/${fileArray[key].split("./").slice(-1)}`
                );
                const copiedFileName = `block/${blockdirs_names[i]}/${uuidv3(
                  crypto
                    .createHash("md5")
                    .update(fileContents)
                    .digest("hex"),
                  "1047a6c8-5c4f-47dc-ad58-981d6396374b"
                )}-${fileArray[key].split("/").slice(-1)}`;
                const textureFileExists = await checkFileExists(
                  `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/${copiedFileName}`
                );
                if (!textureFileExists) {
                  console.log(
                    "\x1b[32m%s\x1b[0m",
                    `\t➡ \x1b[35m[${blockdirs_names[i]}]\x1b[32m Info: /assets/minecraft/textures/block/${blockdirs_names[i]} file does not exist. Copying ...`
                  );

                  await fs.promises.copyFile(
                    `${projectPath}${config.blocks}/${
                      blockdirs_names[i]
                    }/${fileArray[key].split("./").slice(-1)}`,
                    `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/${copiedFileName}`
                  );
                } else {
                  console.log(
                    "\x1b[32m%s\x1b[0m",
                    `\t➡ \x1b[35m[${blockdirs_names[i]}]\x1b[32m Info: /${
                      config.blocks
                    }/${blockdirs_names[i]}/${fileArray[key]
                      .split("/")
                      .slice(-1)} file hasn't changed. Skipping ...`
                  );
                }
                fileArray[key] = copiedFileName.split(".png")[0];
              })()
            )
          );
          return result;
        };

        let parsedBlockConfig = {
          parent: "block/block",
          textures: {},
          elements: [
            {
              from: [0, 0, 0],
              to: [16, 16, 16],
              faces: {
                down: { texture: "#top", cullface: "down" },
                up: { texture: "#bottom", cullface: "up" },
                north: { texture: "#north", cullface: "north" },
                south: { texture: "#south", cullface: "south" },
                west: { texture: "#west", cullface: "west" },
                east: { texture: "#east", cullface: "east" }
              }
            }
          ]
        };
        if (type == "3D") {
          if (!(threeDData instanceof Array)) {
            console.log(
              "\x1b[33m%s\x1b[0m",
              `⚠ Warn: The block folder ${blockdirs_names[i]} has an error in his config "${blockdirs_names[i]}.json". Skipping... `
            );
            return;
          }
          if (
            threeDData
              .map(el => {
                if (!(el instanceof Object)) {
                  return false;
                }
                if (
                  !(el.from instanceof Array) ||
                  !(el.to instanceof Array) ||
                  !(el.faces instanceof Object)
                ) {
                  return false;
                }
                if (
                  Object.keys(el.faces)
                    .map(key => {
                      if (!(el.faces[key].texture.constructor === String)) {
                        return false;
                      }
                      return true;
                    })
                    .some(el => !el)
                ) {
                  return false;
                }
                return true;
              })
              .some(el => !el)
          ) {
            console.log(
              "\x1b[33m%s\x1b[0m",
              `⚠ Warn: The block folder ${blockdirs_names[i]} has an error in his config "${blockdirs_names[i]}.json". Skipping...`
            );
            return;
          } else {
            const textures = {};
            threeDData.forEach(el =>
              Object.keys(el.faces).forEach(key => {
                const texture = el.faces[key].texture;
                textures[
                  `${texture.split("/").slice(-1)}`.split(".png")[0]
                ] = texture;
                el.faces[key].texture = `#${texture
                  .split("/")
                  .slice(-1)}`.split(".png")[0];
              })
            );
            const result = await checkFileArrayExists(textures);
            if (result.some(el => !el)) {
              return;
            }
            await copyFiles(textures);

            parsedBlockConfig.textures = textures;
            parsedBlockConfig.elements = threeDData;
          }
        } else if (type == "unique") {
          const copiedFileNames_1 = {
            top: topFile,
            bottom: bottomFile,
            north: northFile,
            south: southFile,
            east: eastFile,
            west: westFile
          };
          const filesOk = await checkFileArrayExists(copiedFileNames_1);
          if (filesOk === false) return;
          await copyFiles(copiedFileNames_1);
          parsedBlockConfig.textures = {
            ...copiedFileNames_1,
            particle: copiedFileNames_1.top
          };
        } else if (type == "topBottom-seperated") {
          const copiedFileNames_2 = {
            top: topFile,
            bottom: bottomFile,
            base: file
          };
          await checkFileArrayExists(copiedFileNames_2);
          await copyFiles(copiedFileNames_2);

          parsedBlockConfig.textures = {
            particle: copiedFileNames_2.base,
            top: copiedFileNames_2.top,
            bottom: copiedFileNames_2.bottom,
            north: copiedFileNames_2.base,
            south: copiedFileNames_2.base,
            east: copiedFileNames_2.base,
            west: copiedFileNames_2.base
          };
        } else {
          const copiedFileNames_3 = { base: file };
          await checkFileArrayExists(copiedFileNames_3);
          await copyFiles(copiedFileNames_3);

          parsedBlockConfig.textures = {
            particle: copiedFileNames_3.base,
            top: copiedFileNames_3.base,
            bottom: copiedFileNames_3.base,
            north: copiedFileNames_3.base,
            south: copiedFileNames_3.base,
            east: copiedFileNames_3.base,
            west: copiedFileNames_3.base
          };
        }

        await fs.promises.writeFile(
          `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/models/block/${blockdirs_names[i]}.json`,
          JSON.stringify(parsedBlockConfig)
        );
        console.log(
          "\x1b[32m%s\x1b[0m",
          `\t➡ \x1b[35m[${blockdirs_names[i]}]\x1b[32m Info: Writing Config: /assets/minecraft/models/block/${blockdirs_names[i]}.json ...`
        );
        await fs.promises.writeFile(
          `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/blockstates/${blockdirs_names[i]}.json`,
          JSON.stringify({
            variants: { "": { model: `block/${blockdirs_names[i]}` } }
          })
        );
        console.log(
          "\x1b[32m%s\x1b[0m",
          `\t➡ \x1b[35m[${blockdirs_names[i]}]\x1b[32m Info: Writing Config: /assets/minecraft/blockstates/${blockdirs_names[i]}.json ...`
        );
      })()
    )
  );
};
