const fs = require("fs");

function checkFileExits(dir) {
  return new Promise(async (resolve, reject) => {
    try {
      await fs.promises.access(dir);
      resolve(true);
    } catch (err) {
      resolve(false);
    }
  });
}

exports.bundle = async (dirs, projectPath = "./") => {
  const config = await require("../../getConfig")();

  const customTiles = {};

  const blockFileExists = await checkFileExits(
    `${projectPath}${config.outputDir}/temp/blocks.json`
  );
  const itemFileExists = await checkFileExits(
    `${projectPath}${config.outputDir}/temp/items.json`
  );

  if (blockFileExists) {
    const blockFileContents = await fs.promises.readFile(
      `${projectPath}${config.outputDir}/temp/blocks.json`
    );
    const bfContents = JSON.parse(blockFileContents);
    Object.keys(bfContents).forEach(key => {
      if (customTiles[key] == null) customTiles[key] = [];
      customTiles[key] = [...customTiles[key], ...bfContents[key]];
    });
  }

  if (itemFileExists) {
    const itemFileContents = await fs.promises.readFile(
      `${projectPath}${config.outputDir}/temp/items.json`
    );
    const ifContents = JSON.parse(itemFileContents);
    Object.keys(ifContents).forEach(key => {
      if (customTiles[key] == null) customTiles[key] = [];
      customTiles[key] = [...customTiles[key], ...ifContents[key]];
    });
  }

  await Promise.all(
    Object.keys(customTiles).map(key =>
      (async () => {
        const MINECRAFT_NAMED_ID_IDENTIFIER = "minecraft:";

        const baseItemFileExists = await checkFileExits(
          `${projectPath}${config.outputDir}/${
            dirs.pack
          }/assets/minecraft/models/item/${key.split(
            MINECRAFT_NAMED_ID_IDENTIFIER
          )}.json`
        );
        let baseItemConfig = {
          parent: "item/handheld",
          textures: {
            layer0: `item/${key.split(MINECRAFT_NAMED_ID_IDENTIFIER).slice(-1)}`
          }
        };

        if (baseItemFileExists) {
          baseItemConfig = await fs.promises.readFile(
            `${projectPath}${config.outputDir}/${
              dirs.pack
            }/assets/minecraft/models/item/${key
              .split(MINECRAFT_NAMED_ID_IDENTIFIER)
              .slice(-1)}.json`
          );
        }
        if (!(baseItemConfig.overrides instanceof Array))
          baseItemConfig.overrides = [];

        baseItemConfig.overrides = [
          ...baseItemConfig.overrides,
          ...customTiles[key].map((tile, i) => ({
            predicate: { custom_model_data: i + 1 },
            model: tile.file
          }))
        ];

        await fs.promises.writeFile(
          `${projectPath}${config.outputDir}/${
            dirs.pack
          }/assets/minecraft/models/item/${key
            .split(MINECRAFT_NAMED_ID_IDENTIFIER)
            .slice(-1)}.json`,
          JSON.stringify(baseItemConfig)
        );

        console.log(
          "\x1b[32m%s\x1b[0m",
          `➡ Info: Writing Config: assets/minecraft/models/item/${key
            .split(MINECRAFT_NAMED_ID_IDENTIFIER)
            .slice(-1)}.json ...`
        );
      })()
    )
  );

  const spigotCustomBlocks = {};
  const spigotCustomItems = {};
  Object.keys(customTiles).forEach(key => {
    customTiles[key].forEach((tile, i) => {
      if (
        spigotCustomBlocks[key] == null &&
        customTiles[key].some(el => el.type == "block")
      )
        spigotCustomBlocks[key] = {};
      if (
        spigotCustomItems[key] == null &&
        customTiles[key].some(el => el.type == "item")
      )
        spigotCustomItems[key] = {};

      switch (tile.type) {
        case "block":
          spigotCustomBlocks[key][tile.item] = i + 1;
          break;
        case "item":
          spigotCustomItems[key][tile.item] = i + 1;
          break;
      }
    });
  });

  //#ly console.dir(diamondSwordConfig, { depth: null });
  //#ly console.dir(spigotCustomConfig, { depth: null });

  await fs.promises.writeFile(
    `${projectPath}${config.outputDir}/${dirs.spigot}/customItems.json`,
    JSON.stringify(spigotCustomItems)
  );

  console.log(
    "\x1b[32m%s\x1b[0m",
    `➡ Info: Writing Config: /spigotConfigs/customItems.json ...`
  );

  await fs.promises.writeFile(
    `${projectPath}${config.outputDir}/${dirs.spigot}/customBlocks.json`,
    JSON.stringify(spigotCustomBlocks)
  );

  console.log(
    "\x1b[32m%s\x1b[0m",
    `➡ Info: Writing Config: /spigotConfigs/customBlocks.json ...`
  );
};
