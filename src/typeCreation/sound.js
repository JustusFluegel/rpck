const prompt = require("../prompt");
const fs = require("fs");
const inquirer = require("inquirer");

const notEmptyValidator = n => n.trim() != "";

exports.create = async (args, projectPath = "./") => {
  const config = await require("../getConfig")();
  await require("../checkAndCreateStructureDirs")(config, projectPath);

  const sounddir_files = await fs.promises.readdir(
    `${projectPath}/${config.sounds}`
  );
  const sounddir_files_stats = await Promise.all(
    sounddir_files.map(file =>
      fs.promises.stat(`${projectPath}/${config.sounds}/${file}`)
    )
  );
  const avaivable_namespaces = sounddir_files.filter((_, i) =>
    sounddir_files_stats[i].isDirectory()
  );

  let results = {};

  let lastResult = await inquirer.prompt({
    type: "list",
    name: "namespace",
    message: "Please select the Namespace for the sound you want to create",
    choices: [...avaivable_namespaces, "minecraft", "other"]
  });
  results = { ...results, ...lastResult };

  if (results.namespace == "other") {
    lastResult = await inquirer.prompt({
      name: "namespace",
      message: "Type your custom namespace",
      validate: notEmptyValidator
    });
    results = { ...results, ...lastResult };
  }

  lastResult = await inquirer.prompt({
    name: "soundname",
    message: "Type your soundname",
    validate: notEmptyValidator
  });
  results = { ...results, ...lastResult };

  if (!avaivable_namespaces.includes(results.namespace)) {
    await fs.promises.mkdir(
      `${projectPath}/${config.sounds}/${results.namespace}`
    );

    console.log(
      "\x1b[32m%s\x1b[0m",
      `➡ Info: The folder for the namesapce ${results.namespace} doesn't exist, creating ...`
    );
  } else {
    console.log(
      "\x1b[32m%s\x1b[0m",
      `➡ Info: The folder for the namesapce ${results.namespace} does exist, skipping ...`
    );
  }
  const ex_sounds_files = await fs.promises.readdir(
    `${projectPath}/${config.sounds}/${results.namespace}`
  );
  const ex_sounds_files_stats = await Promise.all(
    ex_sounds_files.map(file =>
      fs.promises.stat(
        `${projectPath}/${config.sounds}/${results.namespace}/${file}`
      )
    )
  );
  const ex_sounds = ex_sounds_files.filter((file, i) =>
    ex_sounds_files_stats[i].isDirectory()
  );

  if (ex_sounds.includes(results.soundname)) {
    return console.log(
      "\x1b[31m%s\x1b[0m",
      `❌ Error: The sound does already exist in this namespace.`
    );
  }

  await fs.promises.mkdir(
    `${projectPath}/${config.sounds}/${results.namespace}/${results.soundname}`
  );

  console.log(
    "\x1b[32m%s\x1b[0m",
    `➡ Info: Creating folder for sound ${results.soundname} ...`
  );

  await fs.promises.writeFile(
    `${projectPath}/${config.sounds}/${results.namespace}/${results.soundname}.json`,
    JSON.stringify(
      {
        sound: results.soundname,
        file: `./${results.soundname}.ogg`,
        preload: false,
        loadOnPlay: false
      },
      null,
      "\t"
    )
  );

  console.log(
    "\x1b[32m%s\x1b[0m",
    `➡ Info: Creating config for sound ${results.soundname} ...`
  );

  const audio = require("./sound/longSample");
  const data = audio.replace(/^data:audio\/\w+;base64,/, "");
  let buf = new Buffer.from(data, "base64");

  await fs.promises.writeFile(
    `${projectPath}/${config.sounds}/${results.namespace}/${results.soundname}.ogg`,
    buf
  );

  console.log(
    "\x1b[32m%s\x1b[0m",
    `➡ Info: Creating sample sound file ${results.soundname} ...`
  );
};

exports.bundle = async (dirs, projectPath = "./") => {};
