const prompt = require("../prompt");
const fs = require("fs");
const uuidv3 = require("uuid/v3");
const crypto = require("crypto");

function checkDirExits(dir) {
  return new Promise(async (resolve, reject) => {
    try {
      await fs.promises.access(dir);
      resolve(true);
    } catch (err) {
      resolve(false);
    }
  });
}

const checkFileExists = checkDirExits;

exports.create = async (args, projectPath = "./") => {
  const config = await require("../getConfig")();

  let customitemname = args[0];
  if (customitemname == null) {
    console.log(
      "\x1b[32m%s\x1b[0m",
      "⌨ Please type the custom item id (the part after customitem:) of the item you want to texture:"
    );
    customitemname = await prompt();

    if (customitemname.trim() == "") {
      console.log(
        "\x1b[31m%s\x1b[0m",
        `❌ Error: The custom item id sholdn't be empty.`
      );
      return;
    }
  }

  await require("../checkAndCreateStructureDirs")(config, projectPath);

  const dirExists = await checkDirExits(
    `${projectPath}${config.customItems}/${customitemname}`
  );

  if (dirExists) {
    console.log(
      "\x1b[31m%s\x1b[0m",
      `❌ Error: The custom item does already exist. Please do just edit: \n - ${projectPath}${config.customItems}/${customitemname}/${customitemname}.png \n - ${projectPath}${config.customItems}/${customitemname}/${customitemname}.json`
    );
    return;
  }

  await fs.promises.mkdir(
    `${projectPath}${config.customItems}/${customitemname}`
  );
  await fs.promises.writeFile(
    `${projectPath}${config.customItems}/${customitemname}/${customitemname}.json`,
    JSON.stringify(
      {
        item: customitemname,
        texture: { type: "generated", file: `./${customitemname}.png` }
      },
      null,
      "\t"
    )
  );

  var img =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8\
    /9hAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUU\
    H4woEFQM0iaKKlwAAACRJREFUOMtjYGBg+P//////ZNMUaWZg+M8w6oJRF4y6YJC4A\
    AB8on2forAllwAAAABJRU5ErkJggg==";
  // strip off the data: url prefix to get just the base64-encoded bytes
  var data = img.replace(/^data:image\/\w+;base64,/, "");
  var buf = new Buffer.from(data, "base64");

  await fs.promises.writeFile(
    `${projectPath}${config.customItems}/${customitemname}/${customitemname}.png`,
    buf
  );
};
exports.bundle = async (dirs, projectPath = "./") => {
  const config = await require("../getConfig")();

  await require("./customitem/folder")(dirs, projectPath);

  const itemfiles = await fs.promises.readdir(
    `${projectPath}${config.customItems}/`
  );

  const itemfiles_stats = await Promise.all(
    itemfiles.map(item =>
      fs.promises.stat(`${projectPath}${config.customItems}/${item}`)
    )
  );

  const itemdirs_names = itemfiles.filter((_, i) =>
    itemfiles_stats[i].isDirectory()
  );

  const itemdirs = itemdirs_names.map(
    itemdir => `${projectPath}${config.customItems}/${itemdir}/`
  );

  const checkDirStructure = async (dir, dirname) => {
    const dirContents = await fs.promises.readdir(dir);
    const dirContents_stats = await Promise.all(
      dirContents.map(file => fs.promises.stat(dir + "/" + file))
    );

    if (!dirContents.includes(dirname + ".json")) return false;
    if (dirContents_stats[dirContents.indexOf(dirname + ".json")].isDirectory())
      return false;
    return true;
  };

  const checkItemConfig = async (dir, dirname) => {
    const configContents_String = await fs.promises.readFile(
      dir + dirname + ".json"
    );
    try {
      const configContents = JSON.parse(configContents_String);
      if (configContents.texture == null) return false;
      if (configContents.texture.type == null) return false;
      if (
        configContents.texture.type != "3D" &&
        configContents.texture.type != "unique" &&
        configContents.texture.file == null
      ) {
        return false;
      }
      if (
        configContents.texture.type == "topBottom-seperated" &&
        (configContents.texture.topFile == null ||
          configContents.texture.bottomFile == null)
      )
        return false;
      if (
        configContents.texture.type == "unique" &&
        (configContents.texture.topFile == null ||
          configContents.texture.bottomFile == null ||
          configContents.texture.northFile == null ||
          configContents.texture.westFile == null ||
          configContents.texture.eastFile == null ||
          configContents.texture.southFile == null)
      )
        return false;

      if (
        configContents.texture.type == "3D" &&
        configContents.texture["3DData"] == null
      ) {
        return false;
      }

      return configContents;
    } catch (err) {
      return false;
    }
  };

  console.log("\x1b[32m%s\x1b[0m", `➡ Info: Bundling custom items ...`);

  const customItems = {};

  await Promise.all(
    itemdirs.map((dir, i) =>
      (async () => {
        const dirStructureOk = await checkDirStructure(dir, itemdirs_names[i]);
        if (!dirStructureOk) {
          console.log(
            "\x1b[33m%s\x1b[0m",
            `⚠ Warn: The item folder ${itemdirs_names[i]} hasn't his file ${itemdirs_names[i]}.json. Skipping...`
          );
          return;
        }

        const itemConfig = await checkItemConfig(dir, itemdirs_names[i]);

        if (itemConfig === false) {
          console.log(
            "\x1b[33m%s\x1b[0m",
            `⚠ Warn: The item folder ${itemdirs_names[i]} has an error in his config "${itemdirs_names[i]}.json". Skipping...`
          );
          return;
        }

        const itemConfigTextures = itemConfig.texture;

        const {
          type,
          file,
          topFile,
          bottomFile,
          northFile,
          westFile,
          southFile,
          eastFile
        } = itemConfigTextures;
        const threeDData = itemConfigTextures["3DData"];

        const checkFileArrayExists = async fileArray =>
          Promise.all(
            Object.keys(fileArray).map(key =>
              (async () => {
                const fileExists = await checkFileExists(
                  `${projectPath}${config.customItems}/${
                    itemdirs_names[i]
                  }/${fileArray[key].split("./").slice(-1)}`
                );
                if (!fileExists) {
                  console.log(
                    "\x1b[33m%s\x1b[0m",
                    `\t⚠ \x1b[35m[${
                      itemdirs_names[i]
                    }]\x1b[32m  Warn: The item folder ${
                      itemdirs_names[i]
                    } has an error in his config "${
                      itemdirs_names[i]
                    }.json". The file ${fileArray[key]
                      .split("./")
                      .slice(-1)} doesn't exist. Skipping...`
                  );
                  return false;
                }
                return true;
              })()
            )
          );

        const copyFiles = async fileArray => {
          const dirExists = await checkDirExits(
            `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/customitems/${itemdirs_names[i]}/`
          );
          if (!dirExists) {
            await fs.promises.mkdir(
              `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/customitems/${itemdirs_names[i]}/`
            );
            console.log(
              "\x1b[32m%s\x1b[0m",
              `\t➡ \x1b[35m[${itemdirs_names[i]}]\x1b[32m Info: /assets/minecraft/textures/customitems/${itemdirs_names[i]} folder does not exist. Creating ...`
            );
          } else {
            console.log(
              "\x1b[32m%s\x1b[0m",
              `\t➡ \x1b[35m[${itemdirs_names[i]}]\x1b[32m Info: /assets/minecraft/textures/customitems/${itemdirs_names[i]} folder does exist. Skipping ...`
            );
          }
          const result = await Promise.all(
            Object.keys(fileArray).map(key =>
              (async () => {
                const fileContents = await fs.promises.readFile(
                  `${projectPath}${config.customItems}/${
                    itemdirs_names[i]
                  }/${fileArray[key].split("./").slice(-1)}`
                );
                const copiedFileName = `customitems/${
                  itemdirs_names[i]
                }/${uuidv3(
                  crypto
                    .createHash("md5")
                    .update(fileContents)
                    .digest("hex"),
                  "1047a6c8-5c4f-47dc-ad58-981d6396374b"
                )}-${fileArray[key].split("/").slice(-1)}`;
                const textureFileExists = await checkFileExists(
                  `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/${copiedFileName}`
                );
                if (!textureFileExists) {
                  console.log(
                    "\x1b[32m%s\x1b[0m",
                    `\t➡ \x1b[35m[${itemdirs_names[i]}]\x1b[32m Info: /assets/minecraft/textures/customitem/${itemdirs_names[i]} file does not exist. Copying ...`
                  );

                  await fs.promises.copyFile(
                    `${projectPath}${config.customItems}/${
                      itemdirs_names[i]
                    }/${fileArray[key].split("./").slice(-1)}`,
                    `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/${copiedFileName}`
                  );
                } else {
                  console.log(
                    "\x1b[32m%s\x1b[0m",
                    `\t➡ \x1b[35m[${itemdirs_names[i]}]\x1b[32m Info: /${
                      config.blocks
                    }/${itemdirs_names[i]}/${fileArray[key]
                      .split("/")
                      .slice(-1)} file hasn't changed. Skipping ...`
                  );
                }
                fileArray[key] = copiedFileName.split(".png")[0];
              })()
            )
          );
          return result;
        };

        let parsedItemConfig = {
          parent: "block/block",
          textures: {},
          elements: [
            {
              from: [0, 0, 0],
              to: [16, 16, 16],
              faces: {
                down: { texture: "#top", cullface: "down" },
                up: { texture: "#bottom", cullface: "up" },
                north: { texture: "#north", cullface: "north" },
                south: { texture: "#south", cullface: "south" },
                west: { texture: "#west", cullface: "west" },
                east: { texture: "#east", cullface: "east" }
              }
            }
          ]
        };

        if (type == "3D") {
          if (!(threeDData instanceof Array)) {
            console.log(
              "\x1b[33m%s\x1b[0m",
              `⚠ Warn: The item folder ${itemdirs_names[i]} has an error in his config "${itemdirs_names[i]}.json". Skipping... `
            );
            return;
          }
          if (
            threeDData
              .map(el => {
                if (!(el instanceof Object)) {
                  return false;
                }
                if (
                  !(el.from instanceof Array) ||
                  !(el.to instanceof Array) ||
                  !(el.faces instanceof Object)
                ) {
                  return false;
                }
                if (
                  Object.keys(el.faces)
                    .map(key => {
                      if (!(el.faces[key].texture.constructor === String)) {
                        return false;
                      }
                      return true;
                    })
                    .some(el => !el)
                ) {
                  return false;
                }
                return true;
              })
              .some(el => !el)
          ) {
            console.log(
              "\x1b[33m%s\x1b[0m",
              `⚠ Warn: The item folder ${itemdirs_names[i]} has an error in his config "${itemdirs_names[i]}.json". Skipping...`
            );
            return;
          } else {
            const textures = {};
            threeDData.forEach(el =>
              Object.keys(el.faces).forEach(key => {
                const texture = el.faces[key].texture;
                textures[
                  `${texture.split("/").slice(-1)}`.split(".png")[0]
                ] = texture;
                el.faces[key].texture = `#${texture
                  .split("/")
                  .slice(-1)}`.split(".png")[0];
              })
            );
            const result = await checkFileArrayExists(textures);
            if (result.some(el => !el)) {
              return;
            }
            await copyFiles(textures);

            parsedItemConfig.textures = textures;
            parsedItemConfig.elements = threeDData;
          }
        } else if (type == "unique") {
          const copiedFileNames = {
            top: topFile,
            bottom: bottomFile,
            north: northFile,
            south: southFile,
            east: eastFile,
            west: westFile
          };
          const filesOk = await checkFileArrayExists(copiedFileNames);
          if (filesOk === false) return;
          await copyFiles(copiedFileNames);
          parsedItemConfig.textures = {
            ...copiedFileNames,
            particle: copiedFileNames.top
          };
        } else if (type == "topBottom-seperated") {
          const copiedFileNames = {
            top: topFile,
            bottom: bottomFile,
            base: file
          };
          await checkFileArrayExists(copiedFileNames);
          await copyFiles(copiedFileNames);

          parsedItemConfig.textures = {
            particle: copiedFileNames.base,
            top: copiedFileNames.top,
            bottom: copiedFileNames.bottom,
            north: copiedFileNames.base,
            south: copiedFileNames.base,
            east: copiedFileNames.base,
            west: copiedFileNames.base
          };
        } else if (type == "generated") {
          parsedItemConfig.parent = "item/generated";
          const copiedFileNames = { layer0: file };
          await checkFileArrayExists(copiedFileNames);
          await copyFiles(copiedFileNames);
          parsedItemConfig.textures = copiedFileNames;
          delete parsedItemConfig.elements;
        } else {
          const copiedFileNames = { base: file };
          await checkFileArrayExists(copiedFileNames);
          await copyFiles(copiedFileNames);

          parsedItemConfig.textures = {
            particle: copiedFileNames.base,
            top: copiedFileNames.base,
            bottom: copiedFileNames.base,
            north: copiedFileNames.base,
            south: copiedFileNames.base,
            east: copiedFileNames.base,
            west: copiedFileNames.base
          };
        }

        await fs.promises.writeFile(
          `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/models/item/customitems/${itemdirs_names[i]}.json`,
          JSON.stringify(parsedItemConfig)
        );

        console.log(
          "\x1b[32m%s\x1b[0m",
          `\t➡ \x1b[35m[${itemdirs_names[i]}]\x1b[32m Info: Writing Config: /assets/minecraft/models/item/customitems/${itemdirs_names[i]}.json ...`
        );

        const MINECRAFT_NAMED_ID_IDENTIFIER = "minecraft:";
        const baseItem =
          MINECRAFT_NAMED_ID_IDENTIFIER +
          (itemConfig.baseItem == null
            ? config.defaultBaseItem
            : itemConfig.baseItem
          )
            .split(MINECRAFT_NAMED_ID_IDENTIFIER)
            .slice(-1);

        if (customItems[baseItem] == null) customItems[baseItem] = [];

        customItems[baseItem].push({
          item: itemConfig.item,
          file: `item/customitems/${itemdirs_names[i]}`,
          type: "item"
        });
      })()
    )
  );
  await fs.promises.writeFile(
    `${projectPath}${config.outputDir}/temp/items.json`,
    JSON.stringify(customItems)
  );
};
