const prompt = require("../prompt");
const fs = require("fs");
const uuidv3 = require("uuid/v3");
const crypto = require("crypto");

function checkDirExits(dir) {
  return new Promise(async (resolve, reject) => {
    try {
      await fs.promises.access(dir);
      resolve(true);
    } catch (err) {
      resolve(false);
    }
  });
}

const checkFileExists = checkDirExits;

exports.create = async (args, projectPath = "./") => {
  const config = await require("../getConfig")();

  let blockid = args[0];
  if (blockid == null) {
    console.log(
      "\x1b[32m%s\x1b[0m",
      "⌨ Please type an named-id for the customblock you want to texture (Spigot plugin: /give @s customblock:yourid):"
    );
    blockid = await prompt();

    if (blockid.trim() == "") {
      console.log("\x1b[31m%s\x1b[0m", `❌ Error: The id sholdn't be empty.`);
      return;
    }
  }

  await require("../checkAndCreateStructureDirs")(config, projectPath);

  const dirExists = await checkDirExits(
    `${projectPath}${config.customBlocks}/${blockid}`
  );

  if (dirExists) {
    console.log(
      "\x1b[31m%s\x1b[0m",
      `❌ Error: The block is already textured. Please do just edit: \n - ${projectPath}${config.customBlocks}/${blockid}/${blockid}.png \n - ${projectPath}${config.customBlocks}/${blockid}/${blockid}.json`
    );
    return;
  }

  await fs.promises.mkdir(`${projectPath}${config.customBlocks}/${blockid}`);
  await fs.promises.writeFile(
    `${projectPath}${config.customBlocks}/${blockid}/${blockid}.json`,
    JSON.stringify(
      {
        item: blockid,
        stackSize: 16,
        "#Stacksizes avaivable": [8, 16, 64],
        texture: {
          type: "same",
          file: `./${blockid}.png`,
          "#Supported Types": ["same", "topBottom-seperated", "unique", "3D"]
        }
      },
      null,
      "\t"
    )
  );

  var img =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8\
    /9hAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUU\
    H4woEFQM0iaKKlwAAACRJREFUOMtjYGBg+P//////ZNMUaWZg+M8w6oJRF4y6YJC4A\
    AB8on2forAllwAAAABJRU5ErkJggg==";

  var data = img.replace(/^data:image\/\w+;base64,/, "");
  var buf = new Buffer.from(data, "base64");

  await fs.promises.writeFile(
    `${projectPath}${config.customBlocks}/${blockid}/${blockid}.png`,
    buf
  );
};
exports.bundle = async (dirs, projectPath = "./") => {
  const config = await require("../getConfig")();

  await require("./customblock/folder")(dirs, projectPath);

  const blockfiles = await fs.promises.readdir(
    `${projectPath}${config.customBlocks}/`
  );
  const blockfiles_stats = await Promise.all(
    blockfiles.map(block =>
      fs.promises.stat(`${projectPath}${config.customBlocks}/${block}`)
    )
  );
  const blockdirs_names = blockfiles.filter((_, i) =>
    blockfiles_stats[i].isDirectory()
  );
  const blockdirs = blockdirs_names.map(
    blockdir => `${projectPath}${config.customBlocks}/${blockdir}/`
  );

  const checkDirStructure = async (dir, dirname) => {
    const dirContents = await fs.promises.readdir(dir);
    const dirContents_stats = await Promise.all(
      dirContents.map(file => fs.promises.stat(dir + "/" + file))
    );

    if (!dirContents.includes(dirname + ".json")) return false;
    if (dirContents_stats[dirContents.indexOf(dirname + ".json")].isDirectory())
      return false;
    return true;
  };

  const checkBlockConfig = async (dir, dirname) => {
    const configContents_String = await fs.promises.readFile(
      dir + dirname + ".json"
    );
    try {
      const configContents = JSON.parse(configContents_String);

      if (configContents.item == null) return false;

      //if (configContents.stackSize == null) return false;
      //TODO: Implement Stack sizes @low
      if (configContents.texture == null) return false;
      if (configContents.texture.type == null) return false;

      if (
        configContents.texture.type != "3D" &&
        configContents.texture.type != "unique" &&
        configContents.texture.file == null
      ) {
        return false;
      }
      if (
        configContents.texture.type == "topBottom-seperated" &&
        (configContents.texture.topFile == null ||
          configContents.texture.bottomFile == null)
      )
        return false;
      if (
        configContents.texture.type == "unique" &&
        (configContents.texture.topFile == null ||
          configContents.texture.bottomFile == null ||
          configContents.texture.northFile == null ||
          configContents.texture.westFile == null ||
          configContents.texture.eastFile == null ||
          configContents.texture.southFile == null)
      )
        return false;

      if (
        configContents.texture.type == "3D" &&
        configContents.texture["3DData"] == null
      ) {
        return false;
      }

      return configContents;
    } catch (err) {
      return false;
    }
  };

  console.log("\x1b[32m%s\x1b[0m", `➡ Info: Bundling custom blocks ...`);

  const customBlocks = {};

  await Promise.all(
    blockdirs.map((dir, i) =>
      (async () => {
        const dirStructureOk = await checkDirStructure(dir, blockdirs_names[i]);
        if (!dirStructureOk) {
          console.log(
            "\x1b[33m%s\x1b[0m",
            `⚠ Warn: The custom block folder ${blockdirs_names[i]} hasn't his file ${blockdirs_names[i]}.json. Skipping...`
          );
          return;
        }

        const blockConfig = await checkBlockConfig(dir, blockdirs_names[i]);

        if (blockConfig === false) {
          console.log(
            "\x1b[33m%s\x1b[0m",
            `⚠ Warn: The custom block folder ${blockdirs_names[i]} has an error in his config "${blockdirs_names[i]}.json". Skipping...`
          );
          return;
        }

        const blockConfigTextures = blockConfig.texture;

        const {
          type,
          file,
          topFile,
          bottomFile,
          northFile,
          westFile,
          southFile,
          eastFile
        } = blockConfigTextures;
        const threeDData = blockConfigTextures["3DData"];

        const checkFileArrayExists = async fileArray =>
          Promise.all(
            Object.keys(fileArray).map(key =>
              (async () => {
                const fileExists = await checkFileExists(
                  `${projectPath}${config.customBlocks}/${
                    blockdirs_names[i]
                  }/${fileArray[key].split("./").slice(-1)}`
                );
                if (!fileExists) {
                  console.log(
                    "\x1b[33m%s\x1b[0m",
                    `\t⚠ \x1b[35m[${
                      blockdirs_names[i]
                    }]\x1b[32m  Warn: The custom block folder ${
                      blockdirs_names[i]
                    } has an error in his config "${
                      blockdirs_names[i]
                    }.json". The file ${fileArray[key]
                      .split("./")
                      .slice(-1)} doesn't exist. Skipping...`
                  );
                  return false;
                }
                return true;
              })()
            )
          );

        const copyFiles = async fileArray => {
          const dirExists = await checkDirExits(
            `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/customblock/${blockdirs_names[i]}/`
          );
          if (!dirExists) {
            await fs.promises.mkdir(
              `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/customblock/${blockdirs_names[i]}/`,
              { recursive: true }
            );
            console.log(
              "\x1b[32m%s\x1b[0m",
              `\t➡ \x1b[35m[${blockdirs_names[i]}]\x1b[32m Info: /assets/minecraft/textures/customblock/${blockdirs_names[i]} folder does not exist. Creating ...`
            );
          } else {
            console.log(
              "\x1b[32m%s\x1b[0m",
              `\t➡ \x1b[35m[${blockdirs_names[i]}]\x1b[32m Info: /assets/minecraft/textures/customblock/${blockdirs_names[i]} folder does exist. Skipping ...`
            );
          }
          const result = await Promise.all(
            Object.keys(fileArray).map(key =>
              (async () => {
                const fileContents = await fs.promises.readFile(
                  `${projectPath}${config.customBlocks}/${
                    blockdirs_names[i]
                  }/${fileArray[key].split("./").slice(-1)}`
                );
                const copiedFileName = `customblock/${
                  blockdirs_names[i]
                }/${uuidv3(
                  crypto
                    .createHash("md5")
                    .update(fileContents)
                    .digest("hex"),
                  "1047a6c8-5c4f-47dc-ad58-981d6396374b"
                )}-${fileArray[key].split("/").slice(-1)}`;
                const textureFileExists = await checkFileExists(
                  `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/${copiedFileName}`
                );
                if (!textureFileExists) {
                  console.log(
                    "\x1b[32m%s\x1b[0m",
                    `\t➡ \x1b[35m[${
                      blockdirs_names[i]
                    }]\x1b[32m Info: /assets/minecraft/textures/customblock/${
                      blockdirs_names[i]
                    }/${fileArray[key]
                      .split("/")
                      .slice(-1)} file does not exist. Copying ...`
                  );

                  await fs.promises.copyFile(
                    `${projectPath}${config.customBlocks}/${
                      blockdirs_names[i]
                    }/${fileArray[key].split("./").slice(-1)}`,
                    `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/textures/${copiedFileName}`
                  );
                } else {
                  console.log(
                    "\x1b[32m%s\x1b[0m",
                    `\t➡ \x1b[35m[${blockdirs_names[i]}]\x1b[32m Info: /${
                      config.customBlocks
                    }/${blockdirs_names[i]}/${fileArray[key]
                      .split("/")
                      .slice(-1)} file hasn't changed. Skipping ...`
                  );
                }
                fileArray[key] = copiedFileName.split(".png")[0];
              })()
            )
          );
          return result;
        };

        let parsedBlockConfig = {
          display: {
            gui: {
              rotation: [30, 45, 0],
              translation: [0, 0, 0],
              scale: [0.625, 0.625, 0.625]
            },
            ground: {
              rotation: [0, 0, 0],
              translation: [0, 3, 0],
              scale: [0.25, 0.25, 0.25]
            },
            fixed: {
              rotation: [0, 180, 0],
              translation: [0, 0, 0],
              scale: [1, 1, 1]
            },
            head: {
              rotation: [-30, 0, 0],
              translation: [0, -30.75, -7.25],
              scale: [3.025, 3.025, 3.025]
            },
            firstperson_righthand: {
              rotation: [0, 315, 0],
              translation: [0, 2.5, 0],
              scale: [0.4, 0.4, 0.4]
            },
            thirdperson_righthand: {
              rotation: [75, 315, 0],
              translation: [0, 2.5, 0],
              scale: [0.375, 0.375, 0.375]
            }
          },
          textures: {},
          elements: [
            {
              from: [0, 0, 0],
              to: [16, 16, 16],
              faces: {
                down: { texture: "#top", cullface: "down" },
                up: { texture: "#bottom", cullface: "up" },
                north: { texture: "#north", cullface: "north" },
                south: { texture: "#south", cullface: "south" },
                west: { texture: "#west", cullface: "west" },
                east: { texture: "#east", cullface: "east" }
              }
            }
          ]
        };
        if (type == "3D") {
          if (!(threeDData instanceof Array)) {
            console.log(
              "\x1b[33m%s\x1b[0m",
              `⚠ Warn: The custom block folder ${blockdirs_names[i]} has an error in his config "${blockdirs_names[i]}.json". Skipping...`
            );
            return;
          }
          if (
            threeDData
              .map(el => {
                if (!(el instanceof Object)) {
                  return false;
                }
                if (
                  !(el.from instanceof Array) ||
                  !(el.to instanceof Array) ||
                  !(el.faces instanceof Object)
                ) {
                  return false;
                }
                if (
                  Object.keys(el.faces)
                    .map(key => {
                      if (!(el.faces[key].texture.constructor === String)) {
                        console.log("!string");
                        return false;
                      }
                      return true;
                    })
                    .some(el => !el)
                ) {
                  return false;
                }
                return true;
              })
              .some(el => !el)
          ) {
            console.log(
              "\x1b[33m%s\x1b[0m",
              `⚠ Warn: The custom block folder ${blockdirs_names[i]} has an error in his config "${blockdirs_names[i]}.json". Skipping...`
            );
            return;
          } else {
            const textures = {};
            threeDData.forEach(el =>
              Object.keys(el.faces).forEach(key => {
                const texture = el.faces[key].texture;
                textures[
                  `${texture.split("/").slice(-1)}`.split(".png")[0]
                ] = texture;
                el.faces[key].texture = `#${texture
                  .split("/")
                  .slice(-1)}`.split(".png")[0];
              })
            );
            const result = await checkFileArrayExists(textures);
            if (result.some(el => !el)) {
              return;
            }
            await copyFiles(textures);

            parsedBlockConfig.textures = textures;
            parsedBlockConfig.elements = threeDData;
          }
        } else if (type == "unique") {
          const copiedFileNames_1 = {
            top: topFile,
            bottom: bottomFile,
            north: northFile,
            south: southFile,
            east: eastFile,
            west: westFile
          };
          const filesOk = await checkFileArrayExists(copiedFileNames_1);
          if (filesOk === false) return;
          await copyFiles(copiedFileNames_1);
          parsedBlockConfig.textures = copiedFileNames_1;
        } else if (type == "topBottom-seperated") {
          const copiedFileNames_2 = {
            top: topFile,
            bottom: bottomFile,
            base: file
          };
          await checkFileArrayExists(copiedFileNames_2);
          await copyFiles(copiedFileNames_2);

          parsedBlockConfig.textures = {
            top: copiedFileNames_2.top,
            bottom: copiedFileNames_2.bottom,
            north: copiedFileNames_2.base,
            south: copiedFileNames_2.base,
            east: copiedFileNames_2.base,
            west: copiedFileNames_2.base
          };
        } else {
          const copiedFileNames_3 = { base: file };
          await checkFileArrayExists(copiedFileNames_3);
          await copyFiles(copiedFileNames_3);

          parsedBlockConfig.textures = {
            top: copiedFileNames_3.base,
            bottom: copiedFileNames_3.base,
            north: copiedFileNames_3.base,
            south: copiedFileNames_3.base,
            east: copiedFileNames_3.base,
            west: copiedFileNames_3.base
          };
        }

        await fs.promises.writeFile(
          `${projectPath}${config.outputDir}/${dirs.pack}/assets/minecraft/models/item/customblocks/${blockdirs_names[i]}.json`,
          JSON.stringify(parsedBlockConfig)
        );

        console.log(
          "\x1b[32m%s\x1b[0m",
          `\t➡ \x1b[35m[${blockdirs_names[i]}]\x1b[32m Info: Writing Config: /assets/minecraft/models/item/customblocks/${blockdirs_names[i]}.json ...`
        );

        const MINECRAFT_NAMED_ID_IDENTIFIER = "minecraft:";
        const baseItem = MINECRAFT_NAMED_ID_IDENTIFIER + "dirt";

        if (customBlocks[baseItem] == null) customBlocks[baseItem] = [];

        customBlocks[baseItem].push({
          item: blockConfig.item,
          file: `item/customblocks/${blockdirs_names[i]}`,
          type: "block"
        });
      })()
    )
  );
  await fs.promises.writeFile(
    `${projectPath}${config.outputDir}/temp/blocks.json`,
    JSON.stringify(customBlocks)
  );
};
