process.stdin.setEncoding("utf-8");

module.exports = () =>
  new Promise((resolve, reject) => {
    const f = data => {
      resolve(data.split("\n")[0]);
    };

    process.stdin.once("data", f);
  });
