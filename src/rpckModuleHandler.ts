import { RpckModuleLogLevel } from "./rpckModuleLoader";
import { RpckDatabaseModuleClass } from ".";

export class RpckModuleHandler {
    protected _messages: Array<{ logLevel: RpckModuleLogLevel; tags: string[]; message: string } | Error> = [];
    protected _defaultLogLevel: RpckModuleLogLevel = RpckModuleLogLevel.INFO;
    protected _logLevel: RpckModuleLogLevel = RpckModuleLogLevel.INFO;
    protected _tags?: string[];
    protected _resolved: boolean = false;
    protected _returnData: { [key: string]: unknown } = {};

    static setupDatabase(message: string, database: RpckDatabaseModuleClass) {
        return { success: true, stepsDone: [{ logLevel: RpckModuleLogLevel.DEBUG, message }], data: database };
    }

    constructor(logLevel: RpckModuleLogLevel = RpckModuleLogLevel.DEBUG) {
        this._logLevel = logLevel;
        this._defaultLogLevel = logLevel;
    }

    addDataEntry<T>(key: string, data: T) {
        this._returnData[key] = data;

        return this;
    }

    addData(dataObject: { [key: string]: unknown }) {
        Object.entries(dataObject).forEach(([key, data]) => this.addDataEntry(key, data));

        return this;
    }

    removeDataEntry(key: string) {
        this._returnData[key] = undefined;

        return this;
    }

    removeData(keys: string[]) {
        keys.forEach(key => this.removeDataEntry(key));

        return this;
    }

    logLevel(logLevel: RpckModuleLogLevel) {
        this._logLevel = logLevel;

        return this;
    }

    debug() {
        return this.logLevel(RpckModuleLogLevel.DEBUG);
    }

    info() {
        return this.logLevel(RpckModuleLogLevel.INFO);
    }

    warn() {
        return this.logLevel(RpckModuleLogLevel.WARN);
    }

    error() {
        return this.logLevel(RpckModuleLogLevel.ERROR);
    }

    tag(tag: string) {
        if (this._tags === undefined) this._tags = [];
        this._tags.push(tag);

        return this;
    }

    tags(tags: string[]) {
        if (this._tags === undefined) this._tags = tags;
        else tags.forEach(tag => this._tags?.push(tag));

        return this;
    }

    log(message: string | Error) {
        if (message instanceof Error) this._messages.push(message);
        else this._messages.push({ logLevel: this._logLevel, tags: this._tags === undefined ? [] : this._tags, message });
        this._logLevel = this._defaultLogLevel;
        this._tags = undefined;
    }

    resolve(success: boolean): { success: boolean; stepsDone: Array<{ message: string; logLevel: RpckModuleLogLevel; tags: string[] } | Error>; data: { [key: string]: unknown } } {
        if (this._resolved) throw new Error("Cant resolve resolved ModuleLogger.");

        return { success, stepsDone: this._messages, data: this._returnData };
    }

    ok(message?: string | Error): { success: boolean; stepsDone: Array<{ message: string; logLevel: RpckModuleLogLevel; tags: string[] } | Error>; data: { [key: string]: unknown } } {
        if (message !== undefined) this.log(message);

        return this.resolve(true);
    }

    fail(message?: string | Error): { success: boolean; stepsDone: Array<{ message: string; logLevel: RpckModuleLogLevel; tags: string[] } | Error>; data: { [key: string]: unknown } } {
        if (message !== undefined) this.error().log(message);

        return this.resolve(false);
    }
}
