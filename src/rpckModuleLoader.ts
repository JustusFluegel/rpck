// eslint-disable-next-line max-classes-per-file
import dependencySolver from "dependency-solver";
import chalk from "chalk";

import { join, dirname } from "path";
import fs from "fs";

import { MaybePromise } from "./utils/maybePromise";
import { config as defaultConfig } from "./defaults";

export interface ParsedRpckConfig {
    name: string;
    description: string;
    out: string;
    dirs: { moduleName: string; ressources: { [key: string]: string } }[];
    rest: { [key: string]: unknown };
}

export interface ParsedRpckConfigWithAbsoluteDirs {
    config: ParsedRpckConfig;
    dirs: { moduleName: string; ressources: { [key: string]: string } }[];
    out: string;
    rootPath: string;
}

export enum RpckModuleGeneratorFunctionType {
    SPIGOT = "spigot",
    RESSOURCE_PACK = "ressourcePack",
    SOURCE = "source",
}

export enum RpckModuleLogLevel {
    ERROR,
    WARN,
    INFO,
    DEBUG,
}

export abstract class RpckDatabaseModuleClass {
    get module(): string | undefined {
        return undefined;
    }
}

export function databaseIsOfType<T extends RpckDatabaseModuleClass>(a: RpckModuleDependencyData): asserts a is T {}

export type RpckModuleDependencyData =
    | {
          [key: string]: unknown;
      }
    | RpckDatabaseModuleClass;

export interface RpckModuleGeneratorFunctionSources {
    [key: string]: string;
}
export interface RpckModuleGeneratorFunctionDependencyData {
    [dependency: string]: RpckModuleDependencyData;
}

export type RpckModuleGeneratorFunction = (
    path: string,
    sources: RpckModuleGeneratorFunctionSources,
    data: { [dependency: string]: RpckModuleDependencyData },
    config: ParsedRpckConfigWithAbsoluteDirs,
    databases: { [databaseModule: string]: RpckModuleDependencyData },
) => MaybePromise<boolean | { success: boolean; stepsDone: Array<Error | string | { message: string; logLevel: RpckModuleLogLevel; tags?: string[] }>; data?: RpckModuleDependencyData }>;
export type RpckModuleGenerator = {
    [key in RpckModuleGeneratorFunctionType | "initDatabase"]?: RpckModuleGeneratorFunction;
};
export interface RpckModuleRessources {
    [key: string]: { description: string; shared?: boolean };
}
export interface RpckModule {
    name: string;
    version: string;
    dependencies?: string[];
    databaseDependencies?: string[];
    standalone?: boolean;
    databaseModule?: boolean;
    ressources?: RpckModuleRessources;
    generators: RpckModuleGenerator;
    supportedVersions?: {
        [key in RpckModuleGeneratorFunctionType]?: string;
    };
}

export class RpckModuleLoader {
    protected _modules: RpckModule[] = [];
    protected _rootPath: string;
    protected _configPath: string;
    protected _logLevel: RpckModuleLogLevel;

    constructor(root: string, config: string = join(root, "rpck.config.json"), logLevel: RpckModuleLogLevel = RpckModuleLogLevel.ERROR) {
        this._rootPath = root;
        this._configPath = config;
        this._logLevel = logLevel;
    }

    addModule(module: RpckModule): void {
        if (module === undefined) throw new Error("Trying to register undefined module. stopping.");
        if (this._modules.some(loadedModule => loadedModule.name === module.name)) {
            console.trace(`Warning: Module '${module.name}' already loaded. Skipping.`);

            return;
        }
        const ressources = this._modules.map(installedModule => ({ ressources: installedModule.ressources == null ? [] : installedModule.ressources, name: installedModule.name }));
        if (
            ressources.some(({ name: moduleName, ressources: moduleRessources }) =>
                Object.entries(moduleRessources).some(
                    ([ressourceName, options]) =>
                        options.shared != null &&
                        !options.shared &&
                        ressources
                            .filter(installedModule => installedModule.name !== moduleName)
                            .map(({ ressources: innerModuleRessources }) => Object.entries(innerModuleRessources))
                            .reduce((a, b) => [...a, ...b], [])
                            .some(([otherRessourceName]) => ressourceName === otherRessourceName),
                ),
            )
        ) {
            console.trace(`Warning: Module '${module.name}' requires conflicting ressources. Skipping.`);

            return;
        }
        this._modules.push(module);
    }
    addModules = (modules: RpckModule[]): void => modules.forEach(module => this.addModule(module));
    checkModule = (name: string): boolean => this._modules.some(module => module.name === name);
    removeModule(name: string): boolean {
        const index = this._modules.findIndex(module => module.name === name);
        if (index === -1) return false;
        this._modules = this._modules.splice(1);

        return true;
    }
    removeModules(names: string[]): boolean | string[] {
        const unremovableModules = names.filter(name => this.removeModule(name));
        if (unremovableModules.length === 0) return true;

        return unremovableModules;
    }
    get modules(): RpckModule[] {
        return this._modules;
    }

    async generate(type?: RpckModuleGeneratorFunctionType) {
        if (type == null) {
            const types = Object.values(RpckModuleGeneratorFunctionType);
            for (const iteratedType of types) if (iteratedType !== "source") await this.generate(iteratedType);

            return;
        }
        if (type !== RpckModuleGeneratorFunctionType.SOURCE) await this.generate(RpckModuleGeneratorFunctionType.SOURCE);

        const conf = await this._checkAndCreateDirs();

        const modulesWithGeneratorForType = this._modules.filter(module => Object.keys(module.generators).includes(type));
        const graph: { [key: string]: string[] } = {};
        modulesWithGeneratorForType.forEach(module => {
            if (module.dependencies != null && module.dependencies.length > 0) graph[module.name] = module.dependencies;
        });

        if (Object.keys(graph).length > 0) {
            graph.root = modulesWithGeneratorForType
                .filter(module => module.standalone || (module.standalone == null && module.ressources != null && Object.keys(module.ressources).length > 0))
                .map(module => module.name);
        }

        let modulesChecked: string[] = [];

        const inRoot = (graphWithRoot: { [module: string]: string[]; root: string[] }, module: string, start: boolean = true): boolean => {
            if (start) modulesChecked = [];
            modulesChecked.push(module);

            return (
                (graphWithRoot.root || []).includes(module) ||
                Object.entries(graphWithRoot)
                    .filter(([_, dependencies]) => dependencies.includes(module))
                    .some(([dependant]) => inRoot(graphWithRoot, dependant, false))
            );
        };

        const availableDatabaseModules = this._modules.filter(module => module.databaseDependencies != null && module.databaseModule);
        const databaseModules = availableDatabaseModules.filter(module =>
            this._modules.some(
                loadedModule =>
                    loadedModule.databaseDependencies != null &&
                    loadedModule.databaseDependencies.includes(module.name) &&
                    inRoot(graph as { [module: string]: string[]; root: string[] }, loadedModule.name),
            ),
        );

        databaseModules.forEach(module => {
            const dependants = this._modules.filter(
                loadedModule =>
                    loadedModule.databaseDependencies != null &&
                    loadedModule.databaseDependencies.includes(module.name) &&
                    inRoot(graph as { [module: string]: string[]; root: string[] }, loadedModule.name),
            );
            if (dependants.length > 0) {
                graph.root.push(module.name);
                graph[module.name] = dependants.map(dependant => dependant.name);
            }
        });

        const removeNotInRoot = (graphWithRoot: { [module: string]: string[]; root: string[] }): { [module: string]: string[]; root: string[] } => {
            const result: { [module: string]: string[]; root: string[] } = { root: graphWithRoot.root };
            Object.entries(graphWithRoot).forEach(([key, value]) => {
                if (key !== "root" && inRoot(graphWithRoot, key)) result[key] = value;
            });

            return result;
        };

        const dependencyTree: string[] = Object.keys(graph).length > 0 ? dependencySolver.solve(removeNotInRoot(graph as { [module: string]: string[]; root: string[] })) : [];

        dependencyTree.forEach(moduleName => {
            const module = this._modules.find(candidateModule => candidateModule.name === moduleName);
            if (module == null) throw new Error(`Error checking database dependencies: Module ${moduleName} not found.`);
            if (module.databaseDependencies != null) {
                const notFoundEntry = module.databaseDependencies.find(dependency => availableDatabaseModules.every(databaseModule => databaseModule.name !== dependency));

                if (notFoundEntry != null)
                    throw new Error(`Error in module ${module}: Database dependency ${notFoundEntry} not found. Remove the module that requires that dependeny or install the dependency.`);
            }
        });

        const notFoundModule = dependencyTree.find(moduleName => !this._modules.some(module => moduleName === module.name) && moduleName !== "root");

        if (notFoundModule !== undefined) throw new Error(`Couldn't find module ${notFoundModule}. Remove the module that depends on it or install it.`);

        const moduleDataFromBuiltModules: { [module: string]: RpckModuleDependencyData } = {};
        const databases: { [module: string]: RpckModuleDependencyData } = {};
        for (const moduleName of databaseModules.map(module => module.name)) await this._handleModule(this._modules, "initDatabase", conf, moduleName, databases);
        for (const moduleName of dependencyTree) await this._handleModule(this._modules, type, conf, moduleName, moduleDataFromBuiltModules, graph, databases);
    }
    // eslint-disable-next-line require-await
    fix = async () => this.generate(RpckModuleGeneratorFunctionType.SOURCE);

    protected async _handleModule(
        modules: RpckModule[],
        type: RpckModuleGeneratorFunctionType | "initDatabase",
        conf: ParsedRpckConfigWithAbsoluteDirs,
        moduleName: string,
        moduleDataFromBuiltModules: { [module: string]: RpckModuleDependencyData },
        graph?: { [key: string]: string[] },
        databases?: { [databaseModule: string]: RpckModuleDependencyData },
    ) {
        if (moduleName === "root") return;
        try {
            const dependencies = graph == null ? [] : graph[moduleName] || [];
            const dependencyData: { [dependency: string]: RpckModuleDependencyData } = {};
            dependencies.forEach(dependency => (dependencyData[this._toCamelCase(dependency)] = {}));
            Object.entries(moduleDataFromBuiltModules)
                .filter(([module]) => dependencies.includes(module))
                .forEach(([module, data]) => {
                    dependencyData[this._toCamelCase(module)] = data;
                });

            const module = modules.find(moduleCandidate => moduleCandidate.name === moduleName);
            const camelCaseModuleName = this._toCamelCase(moduleName);

            const filteredDatabases: { [databaseModule: string]: RpckModuleDependencyData } = {};

            if (databases != null) {
                const databaseDependencies = module?.databaseDependencies || [];
                const databaseModule = module?.databaseModule || false;
                if (databaseModule) filteredDatabases[camelCaseModuleName] = databases[moduleName];
                else {
                    Object.entries(databases)
                        .filter(([database]) => databaseDependencies.includes(database))
                        .forEach(([key, value]) => (filteredDatabases[this._toCamelCase(key)] = value));
                }
            }

            const result = await modules
                .find(moduleCandidate => moduleCandidate.name === moduleName)
                ?.generators[type]?.(
                    type === RpckModuleGeneratorFunctionType.SOURCE || type === "initDatabase" ? this._rootPath : join(conf.out, type),
                    conf.dirs.find(({ moduleName: candidateModuleName }) => candidateModuleName === moduleName)?.ressources || {},
                    dependencyData,
                    conf,
                    filteredDatabases,
                );
            const success = typeof result === "boolean" ? result : result?.success;
            if (typeof result === "object") {
                if (result.data !== undefined) moduleDataFromBuiltModules[moduleName] = result.data;
                result.stepsDone.forEach(step => {
                    if (step instanceof Error) throw step;
                    let logLevel: RpckModuleLogLevel = RpckModuleLogLevel.DEBUG;
                    let message = "";
                    let tags: string[] = [];
                    if (typeof step === "string") message = step;
                    else if (typeof step === "object") {
                        logLevel = step.logLevel;
                        message = step.message;
                        tags = step.tags === undefined ? [] : step.tags;
                    }
                    const parsedTags = tags
                        .map((tag, i) => (i === 0 ? chalk.cyan(`[${tag}]`) : i === 1 ? chalk.magenta(`[${tag}]`) : chalk.white(`[${tag}]`)))
                        .reduce((a, b) => `${a} ${b}`, "")
                        .trim();
                    const tagsString = parsedTags === "" ? "" : ` ${parsedTags}`;
                    switch (this._logLevel) {
                        case RpckModuleLogLevel.DEBUG:
                            // eslint-disable-next-line no-console
                            if (logLevel === RpckModuleLogLevel.DEBUG) console.log(chalk.gray("❓ Debug in module ") + chalk.blue(`{${moduleName}}`) + tagsString + chalk.gray(`: ${message}`));
                        // eslint-disable-next-line no-fallthrough
                        case RpckModuleLogLevel.INFO:
                            if (logLevel === RpckModuleLogLevel.INFO)
                                // eslint-disable-next-line no-console
                                console.log(chalk.green("➡  Info in module  ") + chalk.blue(`{${moduleName}}`) + tagsString + chalk.green(`: ${message.trim()}`));
                        // eslint-disable-next-line no-fallthrough
                        case RpckModuleLogLevel.WARN:
                            if (logLevel === RpckModuleLogLevel.WARN)
                                // eslint-disable-next-line no-console
                                console.log(chalk.yellow("⚠  Warn in module  ") + chalk.blue(`{${moduleName}}`) + tagsString + chalk.yellow(`: ${message.trim()}`));
                        // eslint-disable-next-line no-fallthrough
                        case RpckModuleLogLevel.ERROR:
                            if (this._logLevel === RpckModuleLogLevel.ERROR && logLevel === RpckModuleLogLevel.ERROR && success !== undefined && success) throw new Error(message);
                            if (logLevel === RpckModuleLogLevel.ERROR)
                                // eslint-disable-next-line no-console
                                console.log(chalk.red("✖  Error in module ") + chalk.blue(`{${moduleName}}`) + tagsString + chalk.red(`: ${message.trim()}`));
                            break;
                        default:
                            break;
                    }
                });
            }
            if (success === undefined || !success) throw new Error(`Failed executing generator Function ${type} for module ${moduleName}.`);
        } catch (e) {
            // eslint-disable-next-line no-console
            if (e.message == null) console.log(chalk.red("✖  Error in module ") + chalk.blue(`{${moduleName}}`) + chalk.red(": stopping."));
            else {
                // eslint-disable-next-line no-console
                console.log(
                    chalk.red("✖  Error in module ") +
                        chalk.blue(`{${moduleName}}`) +
                        chalk.red(
                            `: ${(e.message as string).endsWith(".") || (e.message as string).endsWith(".") || (e.message as string).endsWith(".") ? e.message.trim() : `${e.message}.`} stopping.`,
                        ),
                );
            }
            throw e;
        }
    }

    protected _toCamelCase(name: string) {
        return name
            .split("-")
            .map((part, i) => (i === 0 ? part : `${part[0].toUpperCase()}${part.slice(1, part.length)}`))
            .join("");
    }

    protected async _parseConfig(): Promise<ParsedRpckConfig> {
        if (!fs.existsSync(this._configPath)) return { ...defaultConfig, name: dirname(this._rootPath), dirs: [], rest: {} };
        const config = JSON.parse((await fs.promises.readFile(this._configPath)).toString());
        if (config.name == null || typeof config.name !== "string") throw new Error(`Failed parsing config ${this._configPath}. Property (name: string) is required.`);
        if (config.description != null && typeof config.description !== "string") throw new Error(`Failed parsing config ${this._configPath}. Property description needs to be of type string.`);
        if (config.dirs != null && (typeof config.dirs !== "object" || Object.values(config.dirs as { [key: string]: unknown }).some(value => typeof value !== "string")))
            throw new Error(`Failed parsing config ${this._configPath}. Property dirs needs to be of type {[key: string]: string }.`);

        const dirs: { moduleName: string; ressources: { [key: string]: string } }[] = [];

        if (config.dirs != null) {
            const cleanedUpDirEntries = Object.entries(config.dirs as { [key: string]: string }).map(([key, value]) => [key, value.trim().replace(/(?:\/$|\\\\$|\\$)/g, "")]);

            if (cleanedUpDirEntries.some(([origDir, mappedDir]) => cleanedUpDirEntries.filter(([compOrigDir]) => compOrigDir !== origDir).some(([_, compMappedDir]) => compMappedDir === mappedDir)))
                throw new Error(`Failed parsing config ${this._configPath}. Property dirs has duplicate values.`);

            const allDirs = this._modules.map(module => Object.keys(module.ressources == null ? {} : module.ressources)).reduce((a, b) => [...a, ...b], []);
            const configDirNames: { [key: string]: string } = {};
            allDirs.forEach(dirName => (configDirNames[dirName] = this._toCamelCase(dirName)));

            function objectFlip(obj: { [key: string]: string }) {
                const ret: { [key: string]: string } = {};
                Object.keys(obj).forEach(key => {
                    ret[obj[key]] = key;
                });

                return ret;
            }

            const invertedConfigDirNames = objectFlip(configDirNames);

            const keptDirs = this._modules.map(module => ({
                name: module.name,
                ressources: Object.keys(module.ressources == null ? {} : module.ressources)
                    .filter(dir => !cleanedUpDirEntries.some(([key]) => invertedConfigDirNames[key] === dir))
                    .map(dir => dir.trim().replace(/(?:\/$|\\\\$|\\$)/g, "")),
            }));

            if (
                keptDirs
                    .map(({ ressources }) => ressources)
                    .reduce((a, b) => [...a, ...b], [])
                    .some(dir => cleanedUpDirEntries.some(([mappedDir]) => mappedDir === dir))
            )
                throw new Error(`Failed parsing config ${this._configPath}. Property dirs is taking a dir used by a module. Move that dir too or change the related enty.`);

            this._modules.forEach(({ name, ressources }) => {
                const result: { moduleName: string; ressources: { [key: string]: string } } = {
                    moduleName: name,
                    ressources: {},
                };
                keptDirs.find(keptDir => keptDir.name === name)?.ressources.forEach(ressource => (result.ressources[ressource] = ressource));
                (ressources == null ? [] : Object.keys(ressources))
                    .filter(ressource => cleanedUpDirEntries.some(([entry]) => configDirNames[ressource] === entry))
                    .forEach(ressource => (result.ressources[ressource] = cleanedUpDirEntries.find(([entry]) => configDirNames[ressource] === entry)?.[1] || ressource));

                dirs.push(result);
            });
        }

        return {
            name: config.name as string,
            description: config.description == null ? undefined : config.description,
            out: join(this._rootPath, config.out == null ? defaultConfig.out : config.out),
            dirs,
            rest: config,
        };
    }

    protected async _checkAndCreateDirs(): Promise<ParsedRpckConfigWithAbsoluteDirs> {
        if (!fs.existsSync(this._rootPath)) await fs.promises.mkdir(this._rootPath, { recursive: true });
        const config = await this._parseConfig();
        const { dirs: mappedDirs, out } = config;

        const outPath = out.match(/(?:\/|[A-Z]:\\|[A-Z]:\/)/g)?.length === 0 ? join(this._rootPath, out) : out;
        if (!fs.existsSync(outPath)) await fs.promises.mkdir(outPath, { recursive: true });

        await Promise.all(
            Object.values(RpckModuleGeneratorFunctionType)
                .filter(type => type !== RpckModuleGeneratorFunctionType.SOURCE)
                .map(async type => {
                    if (!fs.existsSync(join(outPath, type))) await fs.promises.mkdir(join(outPath, type), { recursive: true });
                }),
        );

        return {
            config,
            out: outPath,
            dirs: await Promise.all(
                mappedDirs.map(async ({ moduleName, ressources }) => {
                    const result: { moduleName: string; ressources: { [key: string]: string } } = {
                        moduleName,
                        ressources: {},
                    };
                    await Promise.all(
                        Object.entries(ressources).map(async ([ressource, actualName]) => {
                            const path = actualName.match(/(?:\/|[A-Z]:\\|[A-Z]:\/)/g)?.length === 0 ? join(this._rootPath, actualName) : actualName;
                            if (!fs.existsSync(path)) await fs.promises.mkdir(path, { recursive: true });
                            result.ressources[ressource] = path;
                        }),
                    );

                    return result;
                }),
            ),
            rootPath: this._rootPath,
        };
    }
}
