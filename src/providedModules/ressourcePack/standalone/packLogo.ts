import { RpckModule, ParsedRpckConfigWithAbsoluteDirs, RpckModuleLogLevel } from "../../../rpckModuleLoader";

import fs from "fs";
import { join } from "path";
import jimp from "jimp";
import mimeTypes from "mime-types";
import { RpckModuleHandler } from "../../../rpckModuleHandler";

const PACK_PNG_DEFAULT_SIZE = 1024;

export const packLogoModule: RpckModule = {
    name: "pack-logo",
    version: "0.0.1",
    standalone: true,
    generators: {
        ressourcePack: async (path: string, sources, dependencyData, conf: ParsedRpckConfigWithAbsoluteDirs) => {
            const moduleLogger = new RpckModuleHandler();
            const logoLocation = conf.config.rest.packLogo;
            if (logoLocation === undefined) return moduleLogger.debug().ok("Skipping pack.png generation as there is no config entry present");
            if (typeof logoLocation !== "string") return moduleLogger.fail("Failed to parse ressource pack logo as config entry packLogo isn't of type string");

            const logoPath = logoLocation.match(/(?:\/|[A-Z]:\\|[A-Z]:\/)/g)?.length === 0 ? join(conf.rootPath, logoLocation) : logoLocation;

            if (!fs.existsSync(logoPath) || (await fs.promises.stat(logoPath)).isDirectory()) return moduleLogger.fail("Failed to find logo file specified in config, or logo file is a directory");

            const mimeType = mimeTypes.lookup(logoPath);

            if (mimeType !== "image/png" && mimeType !== "image/jpeg" && mimeType !== "image/bmp" && mimeType !== "image/tiff")
                return moduleLogger.fail(`Invalid logo mime type ${mimeType}: only jpeg png bmp and tiff are supported`);

            moduleLogger.debug().log("Load Image from source path");
            const packPngPath = join(path, "pack.png");
            const image = (await jimp.read(logoPath)).resize(PACK_PNG_DEFAULT_SIZE, PACK_PNG_DEFAULT_SIZE);

            moduleLogger.debug().log("Check if ressourcePack/pack.png has changed");
            const changed = !fs.existsSync(packPngPath) || (await image.getBase64Async("image/png")) !== (await (await jimp.read(packPngPath)).getBase64Async("image/png"));

            if (changed) {
                moduleLogger.info().log("Copy pack logo to ressourcePack/pack.png");

                await image.writeAsync(packPngPath);
            }

            return moduleLogger.ok();
        },
    },
};
