import { RpckModule, ParsedRpckConfigWithAbsoluteDirs } from "../../../rpckModuleLoader";

import fs from "fs";
import { join } from "path";
import { RpckModuleHandler } from "../../..";

export const packMcMetaModule: RpckModule = {
    name: "pack-mcmeta",
    version: "0.0.1",
    standalone: true,
    generators: {
        ressourcePack: async (path: string, sources, dependencyData, conf: ParsedRpckConfigWithAbsoluteDirs) => {
            const moduleLogger = new RpckModuleHandler();
            const packMcMetaPath = join(path, "pack.mcmeta");

            const newContents = JSON.stringify({ pack: { pack_format: 4, description: conf.config.description } });

            moduleLogger.debug().log("Check if ressourcePack/pack.mcmeta has changed");
            const exists = fs.existsSync(packMcMetaPath);
            if (!exists || JSON.stringify(JSON.parse((await fs.promises.readFile(packMcMetaPath, { encoding: "utf-8" })).toString())) !== newContents) {
                moduleLogger.info().log("Write current config to ressourcePack/pack.mcmeta");
                await fs.promises.writeFile(packMcMetaPath, newContents, { encoding: "utf-8" });
            }

            return moduleLogger.ok();
        },
    },
};
