import {
    RpckModule,
    RpckModuleHandler,
    RpckDatabaseModuleClass,
    RpckModuleLogLevel,
    RpckModuleGeneratorFunctionSources,
    RpckModuleDependencyData,
    ParsedRpckConfigWithAbsoluteDirs,
    databaseIsOfType,
} from "../../..";

export class RpckRessourcePackItemsDatabase extends RpckDatabaseModuleClass {
    protected _tests: string[] = [];
    addItem(item: string) {
        this._tests.push(item);
    }
    generate(rpckModuleHandler: RpckModuleHandler) {
        // eslint-disable-next-line no-console
        this._tests.forEach(test => rpckModuleHandler.debug().tag("tests").log(test));

        return rpckModuleHandler.ok();
    }
}

export const itemsDatabaseModule: RpckModule = {
    name: "items-database",
    version: "0.0.1",
    databaseModule: true,
    generators: {
        initDatabase: () => RpckModuleHandler.setupDatabase("Create new items database", new RpckRessourcePackItemsDatabase()),
        ressourcePack: (
            path: string,
            sources: RpckModuleGeneratorFunctionSources,
            data: { [dependency: string]: RpckModuleDependencyData },
            config: ParsedRpckConfigWithAbsoluteDirs,
            { itemsDatabase: database }: { [databaseModule: string]: RpckModuleDependencyData },
        ) => {
            databaseIsOfType<RpckRessourcePackItemsDatabase>(database);
            const moduleHandler = new RpckModuleHandler();
            if (database == null) return moduleHandler.fail("Failed! Got no database!");

            return database.generate(moduleHandler);
        },
    },
};
