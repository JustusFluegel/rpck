export {
    assetsModule,
    minecraftAssetsModule,
    minecraftModelsAssetsModule,
    minecraftTexturesAssetsModule,
    minecraftItemsAssetsModule,
    minecraftBlocksAssetsModule,
    minecraftCustomblocksAssetsModule,
    minecraftCustomitemsAssetsModule,
} from "./assets";
