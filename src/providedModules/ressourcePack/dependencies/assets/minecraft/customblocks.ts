import { RpckModule, RpckModuleHandler } from "../../../../..";

import fs from "fs";
import { join } from "path";

export const minecraftCustomblocksAssetsModule: RpckModule = {
    name: "minecraft-customblocks-assets",
    version: "0.0.1",
    dependencies: ["minecraft-textures-assets", "minecraft-items-assets"],
    generators: {
        ressourcePack: async (path: string) => {
            const moduleHandler = new RpckModuleHandler();
            const minecraftCustomblockItemModelsAssetsPath = join(path, "assets", "minecraft", "models", "item", "customblocks");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/models/item/customblocks exists");
            if (!fs.existsSync(minecraftCustomblockItemModelsAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/models/item/customblocks");
                await fs.promises.mkdir(minecraftCustomblockItemModelsAssetsPath, { recursive: true });
            }

            const minecraftCustomblockTexturesAssetsPath = join(path, "assets", "minecraft", "textures", "customblocks");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/textures/customblocks exists");
            if (!fs.existsSync(minecraftCustomblockTexturesAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/textures/customblocks");
                await fs.promises.mkdir(minecraftCustomblockTexturesAssetsPath, { recursive: true });
            }

            return moduleHandler.ok();
        },
    },
};
