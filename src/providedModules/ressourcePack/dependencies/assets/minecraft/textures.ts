import { RpckModule, RpckModuleHandler } from "../../../../..";

import fs from "fs";
import { join } from "path";

export const minecraftTexturesAssetsModule: RpckModule = {
    name: "minecraft-textures-assets",
    version: "0.0.1",
    dependencies: ["minecraft-assets"],
    generators: {
        ressourcePack: async (path: string) => {
            const moduleHandler = new RpckModuleHandler();
            const minecraftTexturesAssetsPath = join(path, "assets", "minecraft", "textures");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/textures exists");
            if (!fs.existsSync(minecraftTexturesAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/textures");
                await fs.promises.mkdir(minecraftTexturesAssetsPath, { recursive: true });
            }

            return moduleHandler.ok();
        },
    },
};
