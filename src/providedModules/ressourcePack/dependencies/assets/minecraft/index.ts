import { RpckModule, RpckModuleHandler } from "../../../../..";

import fs from "fs";
import { join } from "path";

export const minecraftAssetsModule: RpckModule = {
    name: "minecraft-assets",
    version: "0.0.1",
    dependencies: ["assets"],
    generators: {
        ressourcePack: async (path: string) => {
            const moduleHandler = new RpckModuleHandler();
            const minecraftAssetsPath = join(path, "assets", "minecraft");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft exists");
            if (!fs.existsSync(minecraftAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft");
                await fs.promises.mkdir(minecraftAssetsPath, { recursive: true });
            }

            return moduleHandler.ok();
        },
    },
};

export { minecraftModelsAssetsModule } from "./models";
export { minecraftTexturesAssetsModule } from "./textures";

export { minecraftItemsAssetsModule } from "./items";
export { minecraftCustomitemsAssetsModule } from "./customitems";
export { minecraftBlocksAssetsModule } from "./blocks";
export { minecraftCustomblocksAssetsModule } from "./customblocks";
