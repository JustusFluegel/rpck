import { RpckModule, RpckModuleHandler } from "../../../../..";

import fs from "fs";
import { join } from "path";

export const minecraftBlocksAssetsModule: RpckModule = {
    name: "minecraft-blocks-assets",
    version: "0.0.1",
    dependencies: ["minecraft-assets", "minecraft-models-assets", "minecraft-textures-assets"],
    generators: {
        ressourcePack: async (path: string) => {
            const moduleHandler = new RpckModuleHandler();
            const minecraftBlockModelsAssetsPath = join(path, "assets", "minecraft", "models", "block");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/models/block exists");
            if (!fs.existsSync(minecraftBlockModelsAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/models/block");
                await fs.promises.mkdir(minecraftBlockModelsAssetsPath, { recursive: true });
            }

            const minecraftBlockTexturesAssetsPath = join(path, "assets", "minecraft", "textures", "block");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/textures/block exists");
            if (!fs.existsSync(minecraftBlockTexturesAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/textures/block");
                await fs.promises.mkdir(minecraftBlockTexturesAssetsPath, { recursive: true });
            }

            const minecraftBlockstatesAssetsPath = join(path, "assets", "minecraft", "blockstates");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/blockstates exists");
            if (!fs.existsSync(minecraftBlockstatesAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/blockstates");
                await fs.promises.mkdir(minecraftBlockstatesAssetsPath, { recursive: true });
            }

            return moduleHandler.ok();
        },
    },
};
