import { RpckModule, RpckModuleHandler } from "../../../../..";

import fs from "fs";
import { join } from "path";

export const minecraftCustomitemsAssetsModule: RpckModule = {
    name: "minecraft-customitems-assets",
    version: "0.0.1",
    dependencies: ["minecraft-textures-assets", "minecraft-items-assets"],
    generators: {
        ressourcePack: async (path: string) => {
            const moduleHandler = new RpckModuleHandler();
            const minecraftCustomitemsModelsAssetsPath = join(path, "assets", "minecraft", "models", "item", "customitems");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/models/item/customitems exists");
            if (!fs.existsSync(minecraftCustomitemsModelsAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/models/item/customitems");
                await fs.promises.mkdir(minecraftCustomitemsModelsAssetsPath, { recursive: true });
            }

            const minecraftCustomitemsTexturesAssetsPath = join(path, "assets", "minecraft", "textures", "customitems");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/textures/customitems exists");
            if (!fs.existsSync(minecraftCustomitemsTexturesAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/textures/customitems");
                await fs.promises.mkdir(minecraftCustomitemsTexturesAssetsPath, { recursive: true });
            }

            return moduleHandler.ok();
        },
    },
};
