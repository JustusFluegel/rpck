import { RpckModule, RpckModuleHandler } from "../../../../..";

import fs from "fs";
import { join } from "path";

export const minecraftItemsAssetsModule: RpckModule = {
    name: "minecraft-items-assets",
    version: "0.0.1",
    dependencies: ["minecraft-models-assets", "minecraft-textures-assets"],
    generators: {
        ressourcePack: async (path: string) => {
            const moduleHandler = new RpckModuleHandler();
            const minecraftItemmodelsAssetsPath = join(path, "assets", "minecraft", "models", "item");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/models/item exists");
            if (!fs.existsSync(minecraftItemmodelsAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/models/item");
                await fs.promises.mkdir(minecraftItemmodelsAssetsPath, { recursive: true });
            }

            const minecraftItemtexturesAssetsPath = join(path, "assets", "minecraft", "textures", "item");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/textures/item exists");
            if (!fs.existsSync(minecraftItemtexturesAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/textures/item");
                await fs.promises.mkdir(minecraftItemtexturesAssetsPath, { recursive: true });
            }

            return moduleHandler.ok();
        },
    },
};
