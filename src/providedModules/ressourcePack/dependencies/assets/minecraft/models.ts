import { RpckModule, RpckModuleHandler } from "../../../../..";

import fs from "fs";
import { join } from "path";

export const minecraftModelsAssetsModule: RpckModule = {
    name: "minecraft-models-assets",
    version: "0.0.1",
    dependencies: ["minecraft-assets"],
    generators: {
        ressourcePack: async (path: string) => {
            const moduleHandler = new RpckModuleHandler();
            const minecraftModelsAssetsPath = join(path, "assets", "minecraft", "models");

            moduleHandler.debug().log("Check if directory ressourcePack/assets/minecraft/models exists");
            if (!fs.existsSync(minecraftModelsAssetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets/minecraft/models");
                await fs.promises.mkdir(minecraftModelsAssetsPath, { recursive: true });
            }

            return moduleHandler.ok();
        },
    },
};
