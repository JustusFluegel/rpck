import { RpckModule, RpckModuleHandler } from "../../../..";

import fs from "fs";
import { join } from "path";

export const assetsModule: RpckModule = {
    name: "assets",
    version: "0.0.1",
    generators: {
        ressourcePack: async (path: string) => {
            const moduleHandler = new RpckModuleHandler();
            const assetsPath = join(path, "assets");

            moduleHandler.debug().log("Check if directory ressourcePack/assets exists");
            if (!fs.existsSync(assetsPath)) {
                moduleHandler.info().log("Create directory ressourcePack/assets");
                await fs.promises.mkdir(assetsPath, { recursive: true });
            }

            return moduleHandler.ok();
        },
    },
};

export {
    minecraftAssetsModule,
    minecraftTexturesAssetsModule,
    minecraftModelsAssetsModule,
    minecraftCustomblocksAssetsModule,
    minecraftBlocksAssetsModule,
    minecraftItemsAssetsModule,
    minecraftCustomitemsAssetsModule,
} from "./minecraft";
