export {
    RpckModuleLoader,
    RpckModule,
    RpckModuleGenerator,
    RpckModuleGeneratorFunction,
    RpckModuleGeneratorFunctionType,
    RpckModuleRessources,
    ParsedRpckConfigWithAbsoluteDirs,
    ParsedRpckConfig,
    RpckModuleDependencyData,
    RpckModuleGeneratorFunctionDependencyData,
    RpckModuleGeneratorFunctionSources,
    RpckModuleLogLevel,
    RpckDatabaseModuleClass,
    databaseIsOfType,
} from "./rpckModuleLoader";
import { RpckModuleLoader, RpckModule } from "./rpckModuleLoader";

export class Rpck extends RpckModuleLoader {}

export {
    packMcMetaModule,
    packLogoModule,
    assetsModule,
    minecraftAssetsModule,
    minecraftModelsAssetsModule,
    minecraftTexturesAssetsModule,
    minecraftCustomblocksAssetsModule,
    minecraftBlocksAssetsModule,
    minecraftItemsAssetsModule,
    minecraftCustomitemsAssetsModule,
    itemsDatabaseModule,
    RpckRessourcePackItemsDatabase,
} from "./providedModules";

import {
    packMcMetaModule,
    packLogoModule,
    assetsModule,
    minecraftAssetsModule,
    minecraftModelsAssetsModule,
    minecraftTexturesAssetsModule,
    minecraftCustomblocksAssetsModule,
    minecraftBlocksAssetsModule,
    minecraftItemsAssetsModule,
    minecraftCustomitemsAssetsModule,
    itemsDatabaseModule,
} from "./providedModules";

export const defaultModules: RpckModule[] = [
    packMcMetaModule,
    assetsModule,
    minecraftAssetsModule,
    minecraftModelsAssetsModule,
    minecraftTexturesAssetsModule,
    packLogoModule,
    minecraftCustomblocksAssetsModule,
    minecraftBlocksAssetsModule,
    minecraftItemsAssetsModule,
    minecraftCustomitemsAssetsModule,
    itemsDatabaseModule,
];

export { RpckModuleHandler } from "./rpckModuleHandler";
