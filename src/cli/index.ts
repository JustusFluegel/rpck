import { Rpck, defaultModules } from "..";
import { RpckModuleLogLevel } from "../rpckModuleLoader";

// eslint-disable-next-line require-await
export async function cli() {
    const rpck = new Rpck("./", undefined, RpckModuleLogLevel.DEBUG);
    rpck.addModules(defaultModules);

    // eslint-disable-next-line no-console
    console.log("generating dir structure ...");
    rpck.generate();
}
