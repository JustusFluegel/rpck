const fs = require("fs");
const prompt = require("./prompt");

module.exports = async args => {
  let skip_config = false;

  args.forEach((v, i) => {
    if (v == "-y" || v == "--yes") {
      skip_config = true;
      args.splice(i, 1);
    }
  });

  const name =
    args[0] == null
      ? await (() => {
          console.log("\x1b[32m%s\x1b[0m", "⌨ Please enter a project name: ");
          return prompt();
        })()
      : args[0];

  if (name.trim() == "") {
    console.log("\x1b[31m%s\x1b[0m", `❌ Name couldn't just use whitespaces`);
    return;
  }

  const files = await fs.promises.readdir("./");
  let dirs = await Promise.all(files.map(f => fs.promises.stat(f)));
  dirs = files.filter((_, i) => dirs[i]);

  if (dirs.includes(name)) {
    console.log(
      "\x1b[31m%s\x1b[0m",
      `❌ Name (Folder) does already exist in this path.`
    );
    return;
  }

  let config = { name };

  if (!skip_config) {
    config.author = await askConfigEntry("Author");
    config.description = await askConfigEntry("Description");

    let answer = "";
    while (answer != "y" && answer != "n") {
      console.log(
        "\x1b[32m%s\x1b[0m",
        "⌨ Do you want to use the default folder configs? (y/n): "
      );
      answer = await prompt();
      answer = answer.trim();
      if (answer == "") answer = "y";
    }

    if (answer != "y") {
      config.items = await askConfigEntry("Items directory");
      config.blocks = await askConfigEntry("Blocks directory");
      config.sounds = await askConfigEntry("Sounds directory");
      config.customItems = await askConfigEntry("Custom items directory");
      config.customGuis = await askConfigEntry("Custom guis directory");
      config.customBlocks = await askConfigEntry("Custom blocks directory");
      config.outputDir = await askConfigEntry("Bundle output directory");
    }
  }

  objRemoveUndefined(config);

  await fs.promises.mkdir(`./${name}`);
  await fs.promises.writeFile(
    `./${name}/rpck.config.json`,
    JSON.stringify(config, null, "\t")
  );

  await require("./checkAndCreateStructureDirs")(config, name);

  if (!skip_config) {
    console.log(
      "\x1b[36m%s\x1b[0m",
      "\n------------------------------------------------------------"
    );
    console.log("\x1b[36m%s\x1b[0m", "Project sucessfuly created. \n");
    console.log(
      "\x1b[36m%s\x1b[0m",
      `To get started, switch to the "${name}" folder with`
    );
    console.log("\x1b[31m%s\x1b[0m", `cd ${name}`);
    console.log("\x1b[36m%s\x1b[0m", `And add / edit files there.`);
    console.log(
      "\x1b[36m%s\x1b[0m",
      "------------------------------------------------------------"
    );
  }
};

async function askConfigEntry(text) {
  console.log("\x1b[32m%s\x1b[0m", `⌨ ${text}: `);
  const input = await prompt();
  if (input == "") return undefined;
  console.log("");
  return input.trim();
}

const objRemoveUndefined = obj =>
  Object.keys(obj).forEach(key =>
    obj[key] === undefined ? delete obj[key] : ""
  );
