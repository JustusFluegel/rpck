const createProject = require("./create");
const bundleProjet = require("./bundle");

const createBlock = require("./typeCreation/block").create;
const createItem = require("./typeCreation/item").create;
const createSound = require("./typeCreation/sound").create;

const createCustomBlock = require("./typeCreation/customblock").create;
const createCustomItem = require("./typeCreation/customitem").create;
const createCustomGui = require("./typeCreation/customgui").create;

const fs = require("fs");

exports.cli = async args => {
  args.splice(0, 2);
  if (args.length >= 1) {
    switch (args[0]) {
      case "generate":
        let contents = await fs.promises.readdir("./");
        contents = contents.filter(file => !file.includes("mcmeta"));
        contents = contents.map(file => file.split(".png")[0]);
        contents[0] = `"${contents[0]}"`;
        console.log(
          "[" + contents.reduce((acc, cur) => `${acc},"${cur}"`) + "]"
        );

        break;
      case "bundle":
      case "b":
        args.splice(0, 1);
        await bundleProjet(args);
        break;
      case "create":
      case "c":
        args.splice(0, 1);
        if (args[0] == null) {
          console.log("\x1b[31m%s\x1b[0m", `❌ Error: 2nd Argument required`);
          break;
        }
        switch (args[0]) {
          case "project":
          case "p":
            args.splice(0, 1);
            await createProject(args);
            break;
          case "custom-block":
          case "cb":
            args.splice(0, 1);
            await createCustomBlock(args);
            break;
          case "custom-item":
          case "ci":
            args.splice(0, 1);
            await createCustomItem(args);
            break;
          case "custom-gui":
          case "cg":
            args.splice(0, 1);
            await createCustomGui(args);
            break;
          case "custom":
          case "c":
            args.splice(0, 1);
            if (args[0] == null) {
              console.log(
                "\x1b[31m%s\x1b[0m",
                `❌ Error: 3rd Argument required`
              );
              break;
            }
            switch (args[0]) {
              case "block":
              case "b":
                args.splice(0, 1);
                await createCustomBlock(args);
                break;
              case "item":
              case "b":
                args.splice(0, 1);
                await createCustomItem(args);
                break;
              case "gui":
              case "b":
                args.splice(0, 1);
                await createCustomGui(args);
                break;
              default:
                console.log(
                  "\x1b[31m%s\x1b[0m",
                  `❌ Error: Invalid Argument "${args[0]}"`
                );
                break;
            }
            break;
          case "sound":
          case "s":
            args.splice(0, 1);
            await createSound(args);
            break;
          case "block":
          case "b":
            args.splice(0, 1);
            await createBlock(args);
            break;
          case "item":
          case "i":
            args.splice(0, 1);
            await createItem(args);
            break;
          default:
            console.log(
              "\x1b[31m%s\x1b[0m",
              `❌ Error: Invalid Argument "${args[0]}"`
            );
            break;
        }
        break;
      case "create-project":
      case "cp":
        args.splice(0, 1);
        await createProject(args);
        break;
      case "create-sound":
      case "cs":
        args.splice(0, 1);
        await createSound(args);
        break;
      case "create-block":
      case "cb":
        args.splice(0, 1);
        await createBlock(args);
        break;
      case "create-item":
      case "ci":
        args.splice(0, 1);
        await createItem(args);
        break;
      case "create-custom-block":
      case "ccb":
        args.splice(0, 1);
        await createCustomBlock(args);
        break;
      case "create-custom-item":
      case "cci":
        args.splice(0, 1);
        await createCustomItem(args);
        break;
      case "create-custom-item":
      case "ccs":
        args.splice(0, 1);
        await createCustomItem(args);
        break;
      case "create-custom-gui":
      case "ccg":
        args.splice(0, 1);
        await createCustomGui(args);
        break;
      default:
        console.log(
          "\x1b[31m%s\x1b[0m",
          `❌ Error: Invalid Argument "${args[0]}"`
        );
        break;
    }
  } else console.log("\x1b[31m%s\x1b[0m", "❌ Error: No action specified");
  process.exit();
};
